#pragma once
template<class Vertex, class Edge> class Graf;
#include "Edge.h"
#include "Vertex.h"
#include "MGraf.h"
#include "LGraf.h"
#include <iostream>
using namespace std;
template<class Vertex, class Edge> class Graf
{
protected:
	int countV;
	int countE;
	bool F;
	bool D;
	Form<Vertex, Edge> * st = NULL;
	
	Vertex** verts = NULL;
	Edge** edges = NULL;
	
	bool isExists(Vertex *v1, Vertex *v2);
public:
	Graf();
	Graf(int VV, bool DD, bool FF);
	Graf(int VV, int EE, bool DD, bool FF);
	Graf(Graf<Vertex, Edge> *G);
	~Graf();
	
	int V();
	int E();
	bool Directed();
	bool Dense();
	double K();
	void ToListGraf();
	void ToMatrixGraf();
	
	Vertex * InsertV();
	bool DeleteV(Vertex *v);
	Edge * InsertE(Vertex *v1, Vertex *v2);
	Edge * InsertE(Vertex *v1, Vertex *v2, int w);
	bool DeleteE(Vertex *v1, Vertex *v2);
	Edge * GetEdge(Vertex *v1, Vertex *v2);
	
	void Print();
	
	class IteratorVertex
	{
		Graf * g;
		int current;
	public:
		IteratorVertex(Graf *gg)
		{
			g = gg;
			current = -1;
		}
		bool beg()
		{
			if(g->countV != 0)
			{
				current = 0;
				return true;
			}
			current = -1;
			return false;
		}
		bool end()
		{
			if(g->countV != 0)
			{
				current = g->countV - 1;
				return true;
			}
			current = -1;
			return false;
		}
		void operator++ (int)
        {
			if(current == -1)
				return;
			current++;
        }
		Vertex& operator *()
		{
			if(current == -1 || current >= g->countV)
				throw 1;
			return *g->verts[current];
		}
	};
	class IteratorEdge
	{
		Graf<Vertex, Edge> *g;
		typedef typename MGraf<Vertex, Edge>::IteratorEdgeM  IteratorEdgeM;
		typedef typename LGraf<Vertex, Edge>::IteratorEdgeL  IteratorEdgeL;
		IteratorEdgeM *iM;
		IteratorEdgeL *iL;
	public:
		IteratorEdge(Graf<Vertex, Edge> *gg)
		{
			g = gg;
			if(g->F)
			{
				iL = new IteratorEdgeL((LGraf<Vertex, Edge>*)g->st);
				iM = NULL;
			}
			else
			{
				iM = new IteratorEdgeM((MGraf<Vertex, Edge>*)g->st);
				iL = NULL;
			}
		}
		bool beg()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorEdgeL((LGraf<Vertex, Edge>*)g->st);
				return iL->beg();
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorEdgeM((MGraf<Vertex, Edge>*)g->st);
				return iM->beg();
			}
		}
		bool end()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorEdgeL((LGraf<Vertex, Edge>*)g->st);
				return iL->end();
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorEdgeM((MGraf<Vertex, Edge>*)g->st);
				return iM->end();
			}
		}
		void operator++ (int)
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorEdgeL((LGraf<Vertex, Edge>*)g->st);
				(*iL)++;
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorEdgeM((MGraf<Vertex, Edge>*)g->st);
				(*iM)++;
			}
		}
		Edge& operator *()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorEdgeL((LGraf<Vertex, Edge>*)g->st);
				try {
					return *(*iL);
				}
				catch (int e) { throw 1; }
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorEdgeM((MGraf<Vertex, Edge>*)g->st);
				try {
					return *(*iM);
				}
				catch (int e) { throw 1; }
			}
		}
		~IteratorEdge()
		{
			if(iL != NULL)
				delete iL;
			if(iM != NULL)
				delete iM;
		}
	};
	class IteratorOutEdge
	{
		Graf<Vertex, Edge> *g;
		Vertex * v;
		typedef typename MGraf<Vertex, Edge>::IteratorOutEdgeM  IteratorOutEdgeM;
		typedef typename LGraf<Vertex, Edge>::IteratorOutEdgeL  IteratorOutEdgeL;
		IteratorOutEdgeM *iM;
		IteratorOutEdgeL *iL;
	public:
		IteratorOutEdge(Graf *gg, Vertex *vv)
		{
			g = gg;
			v = vv;
			if(g->F)
			{
				iL = new IteratorOutEdgeL((LGraf<Vertex, Edge>*)g->st, v);
				iM = NULL;
			}
			else
			{
				iM = new IteratorOutEdgeM((MGraf<Vertex, Edge>*)g->st, v);
				iL = NULL;
			}
		}
		~IteratorOutEdge()
		{
			if(iM != NULL)
				delete iM;
			if(iL != NULL)
				delete iL;
		}
		bool beg()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorOutEdgeL((LGraf<Vertex, Edge>*)g->st, v);
				return iL->beg();
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorOutEdgeM((MGraf<Vertex, Edge>*)g->st, v);
				return iM->beg();
			}
		}
		bool end()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorOutEdgeL((LGraf<Vertex, Edge>*)g->st, v);
				return iL->end();
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorOutEdgeM((MGraf<Vertex, Edge>*)g->st, v);
				return iM->end();
			}
		}
		void operator++ (int)
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorOutEdgeL((LGraf<Vertex, Edge>*)g->st, v);
				(*iL)++;
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorOutEdgeM((MGraf<Vertex, Edge>*)g->st, v);
				(*iM)++;
			}
		}
		Edge& operator *()
		{
			if(g->F)
			{
				if(iM != NULL)
				{
					delete iM;
					iM = NULL;
				}
				if(iL == NULL)
					iL = new IteratorOutEdgeL((LGraf<Vertex, Edge>*)g->st, v);
				try {
					return *(*iL);
				}
				catch (int e) { throw 1; }
			}
			else
			{
				if(iL != NULL)
				{
					delete iL;
					iL = NULL;
				}
				if(iM == NULL)
					iM = new IteratorOutEdgeM((MGraf<Vertex, Edge>*)g->st, v);
				try {
					return *(*iM);
				}
				catch (int e) { throw 1; }
			}
		}
	};
};
template<class Vertex, class Edge> Graf<Vertex, Edge>::Graf()
{
	D = true;
	F = true;
	countV = 0;
	countE = 0;
	
	st = new MGraf<Vertex, Edge>(D);	
}
template<class Vertex, class Edge> Graf<Vertex, Edge>::Graf(int VV, bool DD, bool FF)
{
	D = DD;
	F = FF;
	countV = 0;
	countE = 0;
	
	if(F)
		st = new LGraf<Vertex, Edge>(this);	
	else
		st = new MGraf<Vertex, Edge>(this);	
	
	for(int i = 0; i < VV; ++i)
	{
		InsertV();
		verts[i]->SetName(i);
	}
}
template<class Vertex, class Edge> Graf<Vertex, Edge>::Graf(int VV, int EE, bool DD, bool FF)
{
	D = DD;
	F = FF;
	countV = 0;
	countE = 0;

	if(F)
		st = new LGraf<Vertex, Edge>(this);	
	else
		st = new MGraf<Vertex, Edge>(this);	
	
	for(int i = 0; i < VV; ++i)
	{
		InsertV();
		verts[i]->SetName(i);
	}
	int canE = countV * (countV - 1);
	if(!D)
		canE /= 2;
	if(EE >= canE)
	{
		if(D)
		{
			for(int i = 0; i < countV; ++i)
				for(int j = 0; j < countV; ++j)
					InsertE(verts[i], verts[j], rand() % 9 + 1);
		}
		else
		{
			for(int i = 0; i < countV; ++i)
				for(int j = i + 1; j < countV; ++j)
					InsertE(verts[i], verts[j], rand() % 9 + 1);
		}
	}
	else
	{
		for(int i = 0; i < EE; ++i)
		{
			int t1, t2;
			do
			{
				t1 = rand() % countV;
				t2 = rand() % countV;
				while(t1 == t2)
					t2 = rand() % countV;
			}
			while(isExists(verts[t1], verts[t2]));
			InsertE(verts[t1], verts[t2], rand() % 9 + 1);
		}
	}
}
template<class Vertex, class Edge> Graf<Vertex, Edge>::Graf(Graf<Vertex, Edge> *G)
{
	countV = G->V();
	countE = G->E();
	D = G->D;
	F = G->F;
	
	if(F)
		st = new LGraf<Vertex, Edge>(this);	
	else
		st = new MGraf<Vertex, Edge>(this);	
	
	verts = new Vertex*[countV];
	edges = new Edge*[countE];
	
	for(int i = 0; i < countV; ++i)
	{
		verts[i] = G->verts[i];
		st->InsertV(verts[i]);
		verts[i]->index = i;
	}
	for(int i = 0; i < countE; ++i)
	{	
		edges[i] = G->edges[i];
		st->InsertE(edges[i]->v1, edges[i]->v2);
	}
	
}
template<class Vertex, class Edge> Graf<Vertex, Edge>::~Graf()
{
	if(countV != 0)
	{
		for(int i = 0; i < countV; ++i)
			delete verts[i];
		delete[] verts;
	}
	if(countE != 0)
	{
		for(int i = 0; i < countE; ++i)
			delete edges[i];
		delete[] edges;
	}
	if(st != NULL)
		delete st;
}
template<class Vertex, class Edge> int Graf<Vertex, Edge>::V()
{
	return countV;
}
template<class Vertex, class Edge> int Graf<Vertex, Edge>::E()
{
	return countE;
}
template<class Vertex, class Edge> bool Graf<Vertex, Edge>::Directed()
{
	return D;
}
template<class Vertex, class Edge> bool Graf<Vertex, Edge>::Dense()
{
	return F;
}
template<class Vertex, class Edge> double Graf<Vertex, Edge>::K()
{
	double canCount = countV * (countV - 1);
	if(!D)
		canCount /= 2;
	if(canCount == 0)
		return -1;
	return countE / canCount;
}
template<class Vertex, class Edge> void Graf<Vertex, Edge>::ToListGraf()
{
	if(F)
		return;
	delete st;
	F = true;
	st = new LGraf<Vertex, Edge>(this);
	for(int i = 0; i < countV; ++i)
		st->InsertV(verts[i]);
	for(int i = 0; i < countE; ++i)
		st->InsertE(edges[i]->v1, edges[i]->v2);
}
template<class Vertex, class Edge> void Graf<Vertex, Edge>::ToMatrixGraf()
{
	if(!F)
		return;
	delete st;
	F = false;
	st = new MGraf<Vertex, Edge>(this);
	for(int i = 0; i < countV; ++i)
		st->InsertV(verts[i]);
	for(int i = 0; i < countE; ++i)
		st->InsertE(edges[i]->v1, edges[i]->v2);
}
template<class Vertex, class Edge> Vertex * Graf<Vertex, Edge>::InsertV()
{
	Vertex ** temp;
	temp = new Vertex*[countV + 1];
	for(int i = 0; i < countV; ++i)
		temp[i] = verts[i];
	if(verts != NULL)
		delete[] verts;
	verts = temp;
	verts[countV] = new Vertex();
	st->InsertV(verts[countV]);
	for(int i = 0; i < countV + 1; ++i)
		verts[i]->index = i;
	++countV;
	return verts[countV - 1];
}
template<class Vertex, class Edge> bool Graf<Vertex, Edge>::DeleteV(Vertex * v)
{
	if(v == NULL || countV == 0)
		return false;
	int delInd = v->index;
	
	st->DeleteV(v);
	
	Vertex ** temp;
	temp = new Vertex*[countV - 1];
	
	for(int i = 0, ti = 0; i < countV; ++i)
	{
		if(i == delInd)
			continue;
		temp[ti++] = verts[i];
	}
		
	if(countE != 0)
	{
		int *iMas = NULL, countI = 0;
		for(int i = 0; i < countE; ++i)
		{
			if(edges[i]->v1 == verts[delInd] || edges[i]->v2 == verts[delInd])
			{
				int * tempI = new int[countI + 1];
				for(int ii = 0; ii < countI; ++ii)
					tempI[ii] = iMas[ii];
				if(iMas != NULL)
				{
					delete[] iMas;
					iMas = NULL;
				}
				tempI[countI] = i;
				iMas = tempI;
				++countI;
			}
		}
		
		Edge ** tempE = new Edge*[countE - countI];
		for(int i = 0, ti = 0, tti = 0; i < countE; ++i)
		{
			if(i == iMas[tti++])
			{
				delete edges[i];
				continue;
			}
			tempE[ti++] = edges[i];
		}
		delete[] edges;
		edges = tempE;
		delete[] iMas;
		countE = countE - countI;
	}
	delete verts[delInd];
	delete[] verts;
	verts = temp;
	for(int i = 0; i < countV - 1; ++i)
		verts[i]->index = i;
	--countV;
	return true;
}
template<class Vertex, class Edge> Edge * Graf<Vertex, Edge>::InsertE(Vertex *v1, Vertex *v2)
{
	if(v1 == NULL || v2 == NULL || isExists(v1,v2))
		return NULL;
	st->InsertE(v1, v2);
	
	Edge** temp;
	temp = new Edge*[countE + 1];
	for(int i = 0; i < countE; ++i)
		temp[i] = edges[i];
	if(edges != NULL)
		delete[] edges;
	edges = temp;
	edges[countE] = new Edge(v1, v2);
	
	++countE;
	return edges[countE - 1];
}
template<class Vertex, class Edge> Edge * Graf<Vertex, Edge>::InsertE(Vertex *v1, Vertex *v2, int w)
{
	if(v1 == NULL || v2 == NULL || isExists(v1,v2))
		return NULL;
	st->InsertE(v1, v2);
	Edge** temp;
	temp = new Edge*[countE + 1];
	for(int i = 0; i < countE; ++i)
		temp[i] = edges[i];
	if(edges != NULL)
		delete[] edges;
	edges = temp;
	edges[countE] = new Edge(v1, v2, w);
	
	++countE;
	return edges[countE - 1];
}
template<class Vertex, class Edge> bool Graf<Vertex, Edge>::DeleteE(Vertex * v1, Vertex * v2)
{
	if(v1 == NULL || v2 == NULL || countE == 0)
		return false;
	int delInd = 0;
	for(; delInd < countE; ++delInd)
		if((edges[delInd]->v1 == v1 && edges[delInd]->v2 == v2) || (!D && (edges[delInd]->v1 == v2 && edges[delInd]->v2 == v1)))
			break;
	if(delInd == countE)
		return false;
	
	st->DeleteE(v1, v2);
	
	Edge** temp;
	temp = new Edge*[countE - 1];
	
	for(int i = 0, ti = 0; i < countE; ++i)
	{
		if(i == delInd)
			continue;
		temp[ti++] = edges[i];
	}
	
	delete edges[delInd];
	delete[] edges;
	edges = temp;
	--countE;
	return true;
}
template<class Vertex, class Edge> Edge * Graf<Vertex, Edge>::GetEdge(Vertex * v1, Vertex * v2)
{
	if(v1 == NULL || v2 == NULL || countE == 0)
		return NULL;
	int ind = 0;
	for(; ind < countE; ++ind)
		if((edges[ind]->v1 == v1 && edges[ind]->v2 == v2) || (!D && (edges[ind]->v1 == v2 && edges[ind]->v2 == v1)))
			break;
	if(ind == countE)
	{
		return NULL;
	}
	return edges[ind];
}
template<class Vertex, class Edge> bool Graf<Vertex, Edge>::isExists(Vertex * v1, Vertex * v2)
{
	if(countE == 0)
		return false;
	if(v1 == v2)
		return true;
	for(int i = 0; i < countE; ++i)
		if((edges[i]->v1 == v1 && edges[i]->v2 == v2) || (!D && edges[i]->v1 == v2 && edges[i]->v2 == v1))
			return true;
	return false;
}
template<class Vertex, class Edge> void Graf<Vertex, Edge>::Print()
{
	if(countV == 0)
		cout << "Graph is empty!" << endl;
	else st->Print();
}