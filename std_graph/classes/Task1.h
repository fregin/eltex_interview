#pragma once
template<class Vertex, class Edge> class Task1
{
	typedef typename Graf<Vertex, Edge>::IteratorEdge IteratorEdge; 
	
	int ** result;
	int countV;
	Graf<Vertex, Edge> * g;
	
	void Compute();
public:
	Task1(Graf<Vertex, Edge> *gg);
	Task1(Task1 *T);
	~Task1();
	void Set(Graf<Vertex, Edge> * gg);
	void Restart();
	int ** Result();
};
template<class Vertex, class Edge> Task1<Vertex, Edge>::Task1(Graf<Vertex, Edge> *gg)
{
	g = gg;
	result = NULL;
	countV = g->V();
}
template<class Vertex, class Edge> Task1<Vertex, Edge>::Task1(Task1 *T)
{
	g = T->g;
	result = NULL;
	countV = g->V();
}
template<class Vertex, class Edge> Task1<Vertex, Edge>::~Task1()
{
	if(result != NULL)
	{
		for(int i = 0; i < countV; ++i)
			delete[] result[i];
		delete[] result;
	}
}
template<class Vertex, class Edge> void Task1<Vertex, Edge>::Set(Graf<Vertex, Edge> * gg)
{
	g = gg;
	if(result != NULL)
	{
		for(int i = 0; i < countV; ++i)
			delete[] result[i];
		delete[] result;
	}
	result = NULL;
	countV = g->V();
	Compute();
}
template<class Vertex, class Edge> void Task1<Vertex, Edge>::Restart()
{
	if(result != NULL)
	{
		for(int i = 0; i < countV; ++i)
			delete[] result[i];
		delete[] result;
	}
	result = NULL;
	Compute();
}
template<class Vertex, class Edge> int ** Task1<Vertex, Edge>::Result()
{
	return result;
}
template<class Vertex, class Edge> void Task1<Vertex, Edge>::Compute()
{
	IteratorEdge *iE;
	
	
	int ** matrix, ** d;	//матрица смежности и матрица длин
	
	result = new int*[countV];	//матрица путей
	d = new int*[countV];
	matrix = new int*[countV];
	for(int i = 0; i < countV; ++i)
	{
		result[i] = new int[countV];
		matrix[i] = new int[countV];
		d[i] = new int[countV];
	}
	
	for(int i = 0; i < countV; ++i)
	{
		for(int j = 0; j < countV; ++j)
		{
			if(i == j)
			{
				d[i][j] = 0;
				matrix[i][j] = 1;
				result[i][j] = i;
			}
			else 
			{
				d[i][j] = countV;
				matrix[i][j] = 0;
				result[i][j] = -1;
			}
			
		}
	}
	
	iE = new IteratorEdge(g);
	iE->beg();
	int countE = g->E(), i = 0;
	while(i < countE)
	{
		matrix[(**iE).v1->index][(**iE).v2->index] = 1;
		(*iE)++;
		i++;
	}
	
	int v[countV], temp;
	int minIndex, min;
	for(int ALL = 0; ALL < countV; ++ALL)
	{
		for(int i = 0; i < countV; ++i)
			v[i] = 1;
		d[ALL][ALL] = 0;
		do
		{
			minIndex = countV;
			min = countV;
			for(int i = 0; i < countV; ++i)
			{
				if(v[i] == 1 && d[ALL][i] < min)
				{
					min = d[ALL][i];
					minIndex = i;
				}
			}
			if(minIndex != countV)
			{
				for(int i = 0; i < countV; ++i)
				{
					if(matrix[minIndex][i] == 1)
					{
						temp = min + 1; //matrix[minIndex][i]
						if(temp < d[ALL][i])
						{
							d[ALL][i] = temp;
							result[ALL][i] = minIndex;
						}
					}
				}
				v[minIndex] = 0;
			}
		}
		while(minIndex < countV);
	}
	for(int i = 0; i < countV; ++i)
		for(int j = 0; j < countV; ++j)
			if(result[i][j] == countV)
				result[i][j] = -1;
	for(int i = 0; i < countV; ++i)
		delete[] matrix[i];
	delete[] matrix;
	delete iE;
}