template<class Vertex, class W, class D> class Edge
{
protected:
	W w;
	D data;
public:
	Vertex *v1;
	Vertex *v2;
	Edge();
	Edge(Vertex *vv1, Vertex *vv2);
	Edge(Vertex *vv1, Vertex *vv2, W ww);
	Edge(Vertex *vv1, Vertex *vv2, W ww, D d);
	
	Vertex * V1();
	Vertex * V2();
	W GetW();
	void SetW(W ww);
	D GetData();
	void SetData(D d);
	
	Edge<Vertex, W, D>& operator=(const Edge<Vertex, W, D>& right) 
	{
        if (this == &right)
            return *this;
        w = right.w;
		data = right.data;
		v1 = right.v1;
		v2 = right.v2;
        return *this;
    }
};
template<class Vertex, class W, class D>  Edge<Vertex, W, D>::Edge()
{
	w = 0;
	data = 0;
	v1 = 0;
	v2 = 0;
}
template<class Vertex, class W, class D>  Edge<Vertex, W, D>::Edge(Vertex *vv1, Vertex *vv2)
{
	w = 0;
	data = 0;
	v1 = vv1;
	v2 = vv2;
}
template<class Vertex, class W, class D>  Edge<Vertex, W, D>::Edge(Vertex *vv1, Vertex *vv2, W ww)
{
	data = 0;
	v1 = vv1;
	v2 = vv2;
	w = ww;
}
template<class Vertex, class W, class D>  Edge<Vertex, W, D>::Edge(Vertex *vv1, Vertex *vv2, W ww, D d)
{
	v1 = vv1;
	v2 = vv2;
	w = ww;
	data = d;
}
template<class Vertex, class W, class D>  Vertex * Edge<Vertex, W, D>::V1()
{
	return v1;
}
template<class Vertex, class W, class D>  Vertex * Edge<Vertex, W, D>::V2()
{
	return v2;
}
template<class Vertex, class W, class D>  W Edge<Vertex, W, D>::GetW()
{
	return w;
}
template<class Vertex, class W, class D>  void Edge<Vertex, W, D>::SetW(W ww)
{
	w = ww;
}
template<class Vertex, class W, class D>  D Edge<Vertex, W, D>::GetData()
{
	return data;
}
template<class Vertex, class W, class D>  void Edge<Vertex, W, D>::SetData(D d)
{
	data = d;
}