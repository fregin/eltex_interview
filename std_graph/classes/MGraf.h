#pragma once
#include "Form.h"
template<class Vertex, class Edge> class MGraf : public Form<Vertex, Edge>
{
protected:
	using Form<Vertex, Edge>::D;
	using Form<Vertex, Edge>::countV;
	using Form<Vertex, Edge>::dad;
	Vertex ** matrix;
public:
	MGraf(Graf<Vertex, Edge> *g);
	~MGraf();
	void InsertV(Vertex * v);
	void DeleteV(Vertex * v);
	void InsertE(Vertex * v1, Vertex * v2);
	void DeleteE(Vertex * v1, Vertex * v2);
	
	void Print();
	
	class IteratorEdgeM
	{
		MGraf<Vertex, Edge> * g;
		int currentI, currentJ;
	public:
		IteratorEdgeM(MGraf<Vertex, Edge> *gg)
		{
			g = gg;
			currentI = -1;
			currentJ = -1;
		}
		bool beg()
		{
			for(currentI = 0; currentI < g->countV; ++currentI)
			{
				for(currentJ = 0; currentJ < g->countV; ++currentJ)
				{
					if(g->matrix[currentI * g->countV + currentJ] != NULL && currentI != currentJ)
						return true;
				}
			}
			currentI = -1;
			currentJ = -1;
			return false;
		}
		bool end()
		{
			for(currentI = g->countV - 1; currentI >= 0; --currentI)
			{
				for(currentJ = g->countV - 1; currentJ >= 0; --currentJ)
				{
					if(g->matrix[currentI * g->countV + currentJ] != NULL && currentI != currentJ)
						return true;
				}
			}
			currentI = -1;
			currentJ = -1;
			return false;
		}
		void operator++ (int)
		{
			if(currentI == -1 || currentJ == -1)
				return;
			for(; currentI < g->countV; ++currentI)
			{
				for(++currentJ; currentJ < g->countV; ++currentJ)
				{
					if(g->matrix[currentI * g->countV + currentJ] != NULL && currentI != currentJ)
						return;
				}
				currentJ = -1;
			}
			currentI = -1;
			currentJ = -1;
		}
		Edge& operator *()
		{
			if(currentI == -1 || currentJ == -1)
				throw 1;
			return *g->dad->GetEdge(g->matrix[currentI * g->countV + currentI], g->matrix[currentI * g->countV + currentJ]);
		}
	};
	class IteratorOutEdgeM
	{
		MGraf<Vertex, Edge> *g;
		int currentJ;
		Vertex * v;
	public:
		IteratorOutEdgeM(MGraf<Vertex, Edge> *gg, Vertex *vv)
		{
			g = gg;
			v = vv;
			currentJ = -1;			
		}
		bool beg()
		{
			for(currentJ = 0; currentJ < g->countV; ++currentJ)
			{
				if(g->matrix[v->index * g->countV + currentJ] != NULL && v->index != currentJ)
					return true;
			}
			currentJ = -1;
			return false;
		}
		bool end()
		{
			for(currentJ = g->countV; currentJ >= 0; --currentJ)
			{
				if(g->matrix[v->index * g->countV + currentJ] != NULL && v->index != currentJ)
					return true;
			}
			currentJ = -1;
			return false;
		}
		void operator++ (int)
		{
			if(currentJ == -1)
				return;
			for(++currentJ; currentJ < g->countV; ++currentJ)
			{
				if(g->matrix[v->index * g->countV + currentJ] != NULL && v->index != currentJ)
					return;
			}
			currentJ = -1;
		}
		Edge& operator *()
		{
			if(currentJ == -1)
				throw 1;
			return *g->dad->GetEdge(v, g->matrix[v->index * g->countV + currentJ]);
		}
	};
};
template<class Vertex, class Edge> MGraf<Vertex, Edge>::MGraf(Graf<Vertex, Edge> *g)
{
	dad = g;
	D = g->Directed();
	countV = 0;
	matrix = NULL;
}
template<class Vertex, class Edge> MGraf<Vertex, Edge>::~MGraf()
{
	if(countV != 0)
		delete[] matrix;
	matrix = NULL;
}
template<class Vertex, class Edge> void MGraf<Vertex, Edge>::InsertV(Vertex *v)
{
	Vertex ** temp;
	temp = new Vertex*[(countV + 1) * (countV + 1)];
	for(int i = 0; i < (countV + 1) * (countV + 1); ++i)
			temp[i] = NULL;
	for(int i = 0; i < countV; ++i)
		for(int j = 0; j < countV; ++j)
			temp[i * (countV + 1) + j] = matrix[i * (countV) + j];
		
	temp[countV * (countV + 1) + countV] = v;
	
	delete[] matrix;
	
	++countV;
	matrix = temp;
}
template<class Vertex, class Edge> void MGraf<Vertex, Edge>::DeleteV(Vertex *v)
{
	if(v == NULL)
		return;
	if(countV == 1)
	{
		delete[] matrix;
		matrix = NULL;
		
		countV = 0;		
		return;
	}
	
	Vertex ** temp;
	temp = new Vertex*[(countV - 1) * (countV - 1)];
	for(int i = 0; i < (countV - 1) * (countV - 1); ++i)
			temp[i] = NULL;
		
	for(int i = 0, ti = 0; i < countV; ++i, ++ti)
		for(int j = 0, tj = 0; j < countV; ++j)
		{
			if(i == v->index)
			{
				--ti;
				j = countV;
				continue;
			}
			if(j == v->index)
				continue;
			temp[ti * (countV - 1) + tj++] = matrix[i * countV + j];
		}
		
	delete[] matrix;
	--countV;
	matrix = temp;
}
template<class Vertex, class Edge> void MGraf<Vertex, Edge>::InsertE(Vertex *v1, Vertex *v2)
{
	if(v1 == NULL || v2 == NULL)
		return;
	matrix[v1->index * countV + v2->index] = v2;
	if(!D)
		matrix[v2->index * countV + v1->index] = v1;
}
template<class Vertex, class Edge> void MGraf<Vertex, Edge>::DeleteE(Vertex *v1, Vertex *v2)
{
	if(v1 == NULL || v2 == NULL)
		return;
	matrix[v1->index * countV + v2->index] = NULL;
	if(!D)
		matrix[v2->index * countV + v1->index] = NULL;
}
template<class Vertex, class Edge> void MGraf<Vertex, Edge>::Print()
{
	cout << "\t";
	for(int i = 0; i < countV; ++i)
		cout << matrix[i * countV + i]->GetName() << "\t";
	cout << endl << endl;
	for(int i = 0; i < countV; ++i)
	{	
		cout << matrix[i * countV + i]->GetName() << "\t";
		for(int j = 0; j < countV; ++j)
			cout << (matrix[i*countV + j] != NULL) << "\t";
		cout << endl;
	}
}