#pragma once
#include "Form.h"
template<class Vertex, class Edge> class LGraf : public Form<Vertex, Edge>
{	
protected:
	class Node
	{
	public:
		Vertex * v;
		Node * next;
		Node()
		{
			v = NULL;
			next = NULL;
		}
		Node(Vertex * vv, Node * n)
		{
			v = vv;
			next = n;
		}
	};
	
	using Form<Vertex, Edge>::D;
	using Form<Vertex, Edge>::countV;
	using Form<Vertex, Edge>::dad;
	Node ** list;
public:
	LGraf(Graf<Vertex, Edge> *g);
	~LGraf();
	void InsertV(Vertex * v);
	void DeleteV(Vertex * v);
	void InsertE(Vertex * v1, Vertex * v2);
	void DeleteE(Vertex * v1, Vertex * v2);
	
	void Print();
	
	class IteratorEdgeL
	{
		LGraf<Vertex, Edge> * g;
		int currentI;
		Node * currentN;
	public:
		IteratorEdgeL(LGraf<Vertex, Edge> *gg)
		{
			g = gg;
			currentI = -1;
			currentN = NULL;
		}
		bool beg()
		{
			currentI = 0;
			while(currentI < g->countV && g->list[currentI]->next == NULL)
				currentI++;
			if(currentI == g->countV)
			{
				currentI = -1;
				currentN = NULL;
				return false;
			}
			currentN = g->list[currentI]->next;
			return true;
		}
		bool end()
		{
			currentI = g->countV - 1;
			while(currentI >= 0 && g->list[currentI]->next == NULL)
				currentI--;
			if(currentI == -1)
			{
				currentI = -1;
				currentN = NULL;
				return false;
			}
			currentN = g->list[currentI]->next;
			while(currentN->next != NULL)
				currentN = currentN->next;
			return true;
		}
		void operator++ (int)
		{
			if(currentI == -1)
				return;
			if(currentN->next == NULL)
			{
				while(++currentI < g->countV && g->list[currentI]->next == NULL);
				if(currentI == g->countV)
				{
					currentI = -1;
					currentN = NULL;
					return;
				}	
				currentN = g->list[currentI]->next;
			}
			else
				currentN = currentN->next;
		}
		Edge& operator *()
		{
			if(currentI == -1 || currentI >= g->countV || currentN == NULL)
				throw 1;
			return *g->dad->GetEdge(g->list[currentI]->v, currentN->v);
		}
	};
	class IteratorOutEdgeL
	{
		LGraf<Vertex, Edge> *g;
		Node * currentN;
		Vertex * v;
	public:
		IteratorOutEdgeL(LGraf<Vertex, Edge> *gg, Vertex *vv)
		{
			g = gg;
			v = vv;
			currentN = NULL;			
		}
		bool beg()
		{
			if(g->list[v->index]->next == NULL)
			{
				currentN = NULL;
				return false;
			}
			currentN = g->list[v->index]->next;
			return true;
		}
		bool end()
		{
			if(g->list[v->index]->next == NULL)
			{
				currentN = NULL;
				return false;
			}
			currentN = g->list[v->index]->next;
			while(currentN->next != NULL)
				currentN = currentN->next;
			return true;
		}
		void operator++ (int)
		{
			if(currentN != NULL)
				currentN = currentN->next;
		}
		Edge& operator *()
		{
			if(currentN == NULL)
				throw 1;
			return *g->dad->GetEdge(v, currentN->v);
		}
	};
};

template<class Vertex, class Edge> LGraf<Vertex, Edge>::LGraf(Graf<Vertex, Edge> *g)
{
	dad = g;
	D = g->Directed();
	countV = 0;
	list = NULL;
}
template<class Vertex, class Edge> LGraf<Vertex, Edge>::~LGraf()
{
	if(countV != 0)
	{
		for(int i = 0; i < countV; ++i)
		{
			Node * temp = list[i]->next;
			for(; temp != NULL;)
			{
				Node * t = temp;
				temp = temp->next;
				delete t;
			}
			delete list[i];
		}
		delete[] list;
	}
}
template<class Vertex, class Edge> void LGraf<Vertex, Edge>::InsertV(Vertex *v)
{
	if(v == NULL)
		return;
	Node ** temp;
	temp = new Node*[countV + 1];
	for(int i = 0; i < countV; ++i)
			temp[i] = list[i];

	temp[countV] = new Node(v, NULL);
	
	delete[] list;
	list = temp;
	++countV;
}
template<class Vertex, class Edge> void LGraf<Vertex, Edge>::DeleteV(Vertex *v)
{
	if(v == NULL)
		return;
	if(countV == 1)
	{
		Node * t = list[countV - 1];
		for(; t != NULL;)
		{
			Node * tt = t;
			t = t->next;
			delete tt;
		}
		delete[] list;
		--countV;
		return;
	}
	//удаление ребер
	for(int i = 0; i < countV; ++i)
	{
		Node *t = list[i]->next, *prev = list[i];
		while(t != NULL)
		{
			Node * tt = t;
			t = t->next;
			if(tt->v == v)
			{
				delete tt;
				prev->next = t;
				break;
			}
			prev = tt;
		}	
	}
	
	Node ** temp;
	temp = new Node*[countV - 1];

	for(int i = 0, ti = 0; i < countV; ++i)
	{
		if(list[i]->v == v)
			continue;
		temp[ti++] = list[i];
	}
	delete list[v->index];
	delete[] list;
	
	--countV;
	list = temp;
}
template<class Vertex, class Edge> void LGraf<Vertex, Edge>::InsertE(Vertex *v1, Vertex *v2)
{
	if(v1 == NULL || v2 == NULL)
		return;
	Node *t = list[v1->index];
	while(t->next != NULL)
		t = t->next;
	t->next = new Node(v2, NULL);
	if(!D)
	{
		Node *t = list[v2->index];
		while(t->next != NULL)
			t = t->next;
		t->next = new Node(v1, NULL);
	}
}
template<class Vertex, class Edge> void LGraf<Vertex, Edge>::DeleteE(Vertex *v1, Vertex *v2)
{
	if(v1 == NULL || v2 == NULL)
		return;

	Node *t = list[v1->index]->next, *prev = list[v1->index];
	while(t != NULL)
	{
		Node * tt = t;
		t = t->next;
		if(tt->v == v2)
		{
			delete tt;
			prev->next = t;
			break;
		}
		prev = tt;
	}	
	if(!D)
	{
		t = list[v2->index]->next;
		prev = list[v2->index];
		while(t != NULL)
		{
			Node * tt = t;
			t = t->next;
			if(tt->v == v1)
			{
				delete tt;
				prev->next = t;
				break;
			}
			prev = tt;
		}	
	}
}
template<class Vertex, class Edge> void LGraf<Vertex, Edge>::Print()
{
	for(int i = 0; i < countV; ++i)
	{
		Node * temp = list[i];
		for(; temp != NULL; temp = temp->next)
			cout << temp->v->GetName() << "->";
		cout << endl;
	}
}
