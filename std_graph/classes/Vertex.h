template<class D, class K> class Vertex
{
protected:
	K name;
	D data;
public:
	int index;
	Vertex();
	Vertex(K n, D d);
	
	K GetName();
	D GetData();
	void SetName(K n);
	void SetData(D d);
	
	Vertex<D, K>& operator=(const Vertex<D, K>& right) 
	{
        if (this == &right)
            return *this;
        name = right.name;
		data = right.data;
        return *this;
    }

	bool operator==(const Vertex<D, K>& right) 
	{
		if(name == right.name && data == right.data)
			return true;
		else return false;
	}
	bool operator!=(const Vertex<D, K>& right) 
	{
		if(name == right.name && data == right.data)
			return false;
		else return true;
	}
};
template<class D, class K> Vertex<D, K>::Vertex()
{
	index = -1;
	data = 0;
	name = 0;
}
template<class D, class K> Vertex<D, K>::Vertex(K n, D d)
{
	index = -1;
	data = d;
	name = n;
}
template<class D, class K> K Vertex<D, K>::GetName()
{
	return name;
}
template<class D, class K> D Vertex<D, K>::GetData()
{
	return data;
}
template<class D, class K> void Vertex<D, K>::SetName(K n)
{
	name = n;
}
template<class D, class K> void  Vertex<D, K>::SetData(D d)
{
	data = d;
}