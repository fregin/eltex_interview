#pragma once
#include <vector>
#include <math.h>
template<class Vertex, class Edge> class Task2
{
	typedef typename Graf<Vertex, Edge>::IteratorVertex IteratorVertex; 
	typedef typename Graf<Vertex, Edge>::IteratorEdge IteratorEdge; 
	
	int countV;
	int countE;
	int number;
	Graf<Vertex, Edge> * g;
	Graf<Vertex, Edge> * result;
	int d;
	
	Vertex ** verts;
	Edge ** edges;
	void qsort(int b, int e, Edge ** arr);
	void Compute();
public:
	Task2(Graf<Vertex, Edge> *gg, int dd);
	Task2(Task2 *T);
	~Task2();
	void Set(Graf<Vertex, Edge> * gg, int dd);
	void Restart();
	Graf<Vertex, Edge> * Result();
};
template<class Vertex, class Edge> Task2<Vertex, Edge>::Task2(Graf<Vertex, Edge> *gg, int dd)
{
	g = gg;
	d = dd;
	verts = NULL;
	edges = NULL;
	result = NULL;
	countV = g->V();
	countE = g->E();
}
template<class Vertex, class Edge> Task2<Vertex, Edge>::Task2(Task2 *T)
{
	g = T->g;
	d = T->d;
	verts = NULL;
	edges = NULL;
	result = NULL;
	countV = g->V();
	countE = g->E();
}
template<class Vertex, class Edge> Task2<Vertex, Edge>::~Task2()
{
	if(result != NULL)
		delete result;
}
template<class Vertex, class Edge> void Task2<Vertex, Edge>::Set(Graf<Vertex, Edge> * gg, int dd)
{
	g = gg;
	d = dd;
	countV = g->V();
	countE = g->E();
	if(result != NULL)
	{
		delete result;
		result = NULL;
	}
	Compute();
}
template<class Vertex, class Edge> void Task2<Vertex, Edge>::Restart()
{
	if(result != NULL)
	{
		delete result;
		result = NULL;
	}
	countV = g->V();
	countE = g->E();
	Compute();
}
template<class Vertex, class Edge> Graf<Vertex, Edge> * Task2<Vertex, Edge>::Result()
{
	return result;
}
template<class Vertex, class Edge> void Task2<Vertex, Edge>::qsort(int b, int e, Edge ** arr)
{
	int l = b, r = e;
	Edge * piv = arr[(l + r) / 2];
	while (l <= r)
	{
		while (arr[l]->GetW() < piv->GetW())
			l++;
		while (arr[r]->GetW() > piv->GetW())
			r--;
		if (l <= r)
		{
			Edge * temp = arr[l];
			arr[l++] = arr[r];
			arr[r--] = temp;
		}
	}
	if (b < r)
		qsort (b, r, arr);
	if (e > l)
		qsort (l, e, arr);
}
template<class Vertex, class Edge> void Task2<Vertex, Edge>::Compute()
{
	IteratorVertex iV(g);
	IteratorEdge iE(g);
	verts = new Vertex *[countV];
	edges = new Edge *[countE];
	
	iV.beg();
	iE.beg();
	

	for(int i = 0; i < countV; ++i, iV++)
		verts[i] = &(*iV);

	for(int i = 0, ti = 0; ti < countE; ++i, iE++)
	{
		Edge * temp = &(*iE);
		int j = 0;
		for(; j < ti; ++j)
			if(edges[j] == temp)
				break;
		if(j == ti)
			edges[ti++] = temp;
	}
	
	qsort(0, countE - 1, edges);

	int * setId = new int[countV];
	for(int i = 0; i < countV; ++i)
		setId[i] = i;

	int * edgesNew = new int[countE];
	int * edgesPass = new int[countE];
	int countENew = 0;
	int countEPass = 0;
	
	int minCost = 0;

	int i = 0;
	bool flag = false;
	for (; i < countE && !flag; ++i)
	{
		Vertex * a = edges[i]->v1,  * b = edges[i]->v2;
		if (setId[a->index] != setId[b->index])
		{
			if(fabs(d - minCost) > fabs(d - minCost - edges[i]->GetW()))
			{
				minCost += edges[i]->GetW();
				edgesNew[countENew++] = i;
				int prevSet = setId[b->index],  newSet = setId[a->index];
				for (int j = 0; j < countV; ++j)
					if (setId[j] == prevSet)
						setId[j] = newSet;
			}
			else 
				flag = true;
		}
		else
			edgesPass[countEPass++] = i;		
	}

	if(flag == false)
	{
		for (i = 0; i < countEPass && !flag; ++i)
		{
			Vertex * a = edges[edgesPass[i]]->v1,  * b = edges[edgesPass[i]]->v2;
			if(fabs(d - minCost) >= fabs(d - minCost - edges[edgesPass[i]]->GetW()))
			{
				minCost += edges[edgesPass[i]]->GetW();
				edgesNew[countENew++] = edgesPass[i];
				int prevSet = setId[b->index],  newSet = setId[a->index];
				for (int j = 0; j < countV; ++j)
					if (setId[j] == prevSet)
						setId[j] = newSet;
			}
			else
				flag = true;
		}
	}
	Vertex ** vertsNew = new Vertex*[countV];
	result = new Graf<Vertex, Edge>(0, g->Directed(), g->Dense());
	for(i = 0; i < countV; ++i)
	{
		vertsNew[i] = result->InsertV();
		vertsNew[i]->SetName(verts[i]->GetName());
	}
	for(i = 0; i < countENew; ++i)
		result->InsertE(edges[edgesNew[i]]->v1, edges[edgesNew[i]]->v2);
	
	delete[] edgesPass;
	delete[] edgesNew;
	delete[] setId;
	delete[] verts;
	delete[] edges;
	verts = NULL;
	edges = NULL;
}