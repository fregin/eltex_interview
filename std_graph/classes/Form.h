#pragma once
#include <iostream>
using namespace std;
template<class Vertex, class Edge> class Form
{
protected:
	int countV;
	bool D;
	Graf<Vertex, Edge> *dad;
public:
	virtual void InsertV(Vertex * v) = 0;
	virtual void DeleteV(Vertex * v) = 0;
	virtual void InsertE(Vertex * v1, Vertex * v2) = 0;
	virtual void DeleteE(Vertex * v1, Vertex * v2) = 0;
	
	virtual void Print() = 0;
};