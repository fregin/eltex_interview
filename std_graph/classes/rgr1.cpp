#include "Graf.h"
#include "Task1.h"
#include "Task2.h"
#include <vector>
#include <string.h>
#include <time.h>
void help()
{
	cout << "addver delver\naddedg addedgw deledg getedg\ntolist tomatrix\nvertex edge\n\nv e directed dense k\n\n1 - itVert 2 - itEdge 3 - itOutEdge (3create)\nbeg end ++ get set\nexample: 1beg - itVert begin\n\nhelp print\n";
}
int main()
{
	srand(time(0));
	int v = 0, e = 0;
	bool d = false, f = true;
	cout << "Count Vertexes: ";
	//cin >> v;
	cout << "Count Edges: ";
	//cin >> e;
	cout << "Is Directed?: ";
	//cin >> d;
	cout << "Is List-Graph?: ";
	//cin >> f;
	
	Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> > * g;
	g = new Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> >(v, e, d, f);
	
	Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> >::IteratorVertex iV(g); 
	Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> >::IteratorEdge iE(g); 
	Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> >::IteratorOutEdge * iOE = NULL; 
	Task2<Vertex<int, int>, Edge<Vertex<int, int>, int, int>> * T2 = NULL;
	Task1<Vertex<int, int>, Edge<Vertex<int, int>, int, int>> * T1 = NULL;
	
	Vertex<int,int> ** massV = NULL;
	Edge<Vertex<int, int>, int, int> ** massE = NULL;
	int V = 0; int E = 0;
		
	string cmd;
	help();
	massV = new Vertex<int,int> *[6];
	massE = new Edge<Vertex<int, int>, int, int> *[8];
	V = 6;
	E = 8;
	for(int i = 0; i < V; ++i)
	{
		massV[i] = g->InsertV();
		massV[i]->SetName(i);
	}
	massE[0] = g->InsertE(massV[0], massV[1], 3);
	massE[1] = g->InsertE(massV[1], massV[2], 4);
	massE[2] = g->InsertE(massV[2], massV[3], 1);
	massE[3] = g->InsertE(massV[3], massV[4], 2);
	massE[4] = g->InsertE(massV[4], massV[5], 7);
	massE[5] = g->InsertE(massV[5], massV[0], 4);
	massE[6] = g->InsertE(massV[2], massV[4], 2);
	massE[7] = g->InsertE(massV[1], massV[4], 6);
	
	while (strcmp(cmd.c_str(), "exit") != 0)
	{
		getline(cin, cmd);
		if (strcmp(cmd.c_str(), "addver") == 0)
		{
			int name;
			cin >> name;
			Vertex<int,int> ** temp = new Vertex<int,int> *[V + 1];
			for(int i = 0; i < V; ++i)
				temp[i] = massV[i];
			temp[V] = g->InsertV();
			temp[V++]->SetName(name);
			if(massV != NULL)
				delete[] massV;
			massV = temp;
			continue;
		}
		if (strcmp(cmd.c_str(), "delver") == 0)
		{
			int ind;
			cin >> ind;
			g->DeleteV(massV[ind]);
			Vertex<int,int> ** temp = new Vertex<int,int> *[V - 1];
			for(int i = 0, ti = 0; i < V; ++i)
			{
				if(i == ind)
					continue;
				temp[ti++] = massV[i];
			}
			V--;
			if(massV != NULL)
				delete[] massV;
			massV = temp;
			continue;
		}
		if (strcmp(cmd.c_str(), "addedg") == 0)
		{
			int ind1, ind2;
			cin >> ind1 >> ind2;
			Edge<Vertex<int, int>, int, int> * t = g->InsertE(massV[ind1], massV[ind2]);
			if(t == NULL)
				continue;
			Edge<Vertex<int, int>, int, int> ** temp = new Edge<Vertex<int, int>, int, int> *[E+1];
			for(int i = 0; i < E; ++i)
				temp[i] = massE[i];
			temp[E++] = t;
			if(massE != NULL)
				delete[] massE;
			massE = temp;
			continue;
		}
		if (strcmp(cmd.c_str(), "addedgw") == 0)
		{
			int ind1, ind2, w;
			cin >> ind1 >> ind2 >> w;
			Edge<Vertex<int, int>, int, int> * t = g->InsertE(massV[ind1], massV[ind2], w);
			if(t == NULL)
				continue;
			Edge<Vertex<int, int>, int, int> ** temp = new Edge<Vertex<int, int>, int, int> *[E+1];
			for(int i = 0; i < E; ++i)
				temp[i] = massE[i];
			temp[E++] = t;
			if(massE != NULL)
				delete[] massE;
			massE = temp;
			continue;
		}
		if (strcmp(cmd.c_str(), "deledg") == 0)
		{
			int ind1, ind2, ind;
			cin >> ind1 >> ind2;
			for(ind = 0; ind < E; ++ind)
				if(massE[ind]->v1 == massV[ind1] && massE[ind]->v2 == massV[ind2])
					break;
			g->DeleteE(massV[ind1], massV[ind2]);
			Edge<Vertex<int, int>, int, int> ** temp = new Edge<Vertex<int, int>, int, int> *[E - 1];
			for(int i = 0, ti = 0; i < E; ++i)
			{
				if(i == ind)
					continue;
				temp[ti++] = massE[i];
			}
			E--;
			if(massE != NULL)
				delete[] massE;
			massE = temp;
			continue;
		}
		if (strcmp(cmd.c_str(), "getedg") == 0)
		{
			int ind1, ind2;
			cin >> ind1 >> ind2;
			cout << g->GetEdge(massV[ind1], massV[ind2])->GetW() << endl;
			cout << g->GetEdge(massV[ind1], massV[ind2])->v1->GetName() << " - " << g->GetEdge(massV[ind1], massV[ind2])->v2->GetName() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "print") == 0)
		{
			g->Print();
			continue;
		}
		if (strcmp(cmd.c_str(), "tolist") == 0)
		{
			g->ToListGraf();
			continue;
		}
		if (strcmp(cmd.c_str(), "tomatrix") == 0)
		{
			g->ToMatrixGraf();
			continue;
		}
		if (strcmp(cmd.c_str(), "vertex") ==  0)
		{
			if(V == 0)
				cout << "Empty!\n";
			for(int i = 0; i < V; ++i)
				cout << i << ". " << massV[i]->GetName() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "edge") == 0)
		{
			if(E == 0)
				cout << "Empty!\n";
			for(int i = 0; i < E; ++i)
				cout << i << ". " << massE[i]->v1->GetName() << "->" << massE[i]->v2->GetName()  << " - " << massE[i]->GetW() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "v") ==  0)
		{
			cout << g->V() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "e") ==  0)
		{
			cout << g->E() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "directed") ==  0)
		{
			cout << g->Directed() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "dense") ==  0)
		{
			cout << g->Dense() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "k") ==  0)
		{
			cout << g->K() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "help") ==  0)
		{
			help();
			continue;
		}
		if (strcmp(cmd.c_str(), "1beg") ==  0)
		{
			cout << iV.beg() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "1end") ==  0)
		{
			cout << iV.end() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "1++") ==  0)
		{
			iV++;
			continue;
		}
		if (strcmp(cmd.c_str(), "1set") ==  0)
		{
			try
			{
				int data;
				cin >> data;
				(*iV).SetData(data);
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "1get") ==  0)
		{
			try
			{
				cout << (*iV).GetName() << " - " << (*iV).GetData() << endl;
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "2beg") ==  0)
		{
			cout << iE.beg() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "2end") ==  0)
		{
			cout << iE.end() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "2++") ==  0)
		{
			iE++;
			continue;
		}
		if (strcmp(cmd.c_str(), "2set") ==  0)
		{
			try
			{
				int data;
				cin >> data;
				(*iE).SetData(data);
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "2get") ==  0)
		{
			try
			{
				if(&(*iE)!= NULL)
				{
					cout << (*iE).GetData() << endl;
					cout << (*iE).v1->GetName() << " - " << (*iE).v2->GetName() << endl;
				}
				else
				{
					cout << "EXC: It is off\n";
				}
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "3create") ==  0)
		{
			int ind;
			cin >> ind;
			if(iOE != NULL)
				delete iOE;
			iOE = new Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> >::IteratorOutEdge(g, massV[ind]);
			continue;
		}
		if (strcmp(cmd.c_str(), "3beg") ==  0)
		{
			cout << iOE->beg() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "3end") ==  0)
		{
			cout << iOE->end() << endl;
			continue;
		}
		if (strcmp(cmd.c_str(), "3++") ==  0)
		{
			(*iOE)++;
			continue;
		}
		if (strcmp(cmd.c_str(), "3set") ==  0)
		{
			try
			{
				int data;
				cin >> data;
				(**iOE).SetData(data);
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "3get") ==  0)
		{
			try
			{
				cout << (**iOE).GetData() << endl;
				cout << (**iOE).v1->GetName() << " - " << (**iOE).v2->GetName() << endl;
			}
			catch (int e) { cout << "EXC: It is off\n"; }
			continue;
		}
		if (strcmp(cmd.c_str(), "task1") ==  0)
		{
			T1 = new Task1<Vertex<int, int>, Edge<Vertex<int, int>, int, int>>(g);
			T1->Restart();
			int ** keker = T1->Result();
			cout << "\t";
			for(int i = 0; i < g->V(); ++i)
				cout << i << "\t";
			cout << endl << endl;
			for(int i = 0; i < g->V(); ++i)
			{	
				cout << i << "\t";
				for(int j = 0; j < g->V(); ++j)
					cout << keker[i][j] << "\t";
				cout << endl;
			}
			delete T1;
			T1 = NULL;
			continue;
		}
		if (strcmp(cmd.c_str(), "task2") ==  0)
		{
			int d;
			cin >> d;
			T2 = new Task2<Vertex<int, int>, Edge<Vertex<int, int>, int, int>>(g, d);
			T2->Restart();
			Graf<Vertex<int, int>, Edge<Vertex<int, int>, int, int> > * ans = NULL;
			ans = T2->Result();
			ans->Print();
			delete T2;
			T2 = NULL;
			continue;
		}
		if (strcmp(cmd.c_str(), "") != 0)
			cout << "Wrong command!" << endl;
	}
	delete[] massV;
	delete[] massE;
	return 0;
}