package com.example.jimmy.freginapp;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;

public class StartActivity extends AppCompatActivity {

    protected int current = 1, spys = -1, players = -1;
    ArrayList<Integer> spysId;
    String location;

    TextView playerNumberView;
    TextView locationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        location = "underfined";
        spysId = new ArrayList<>();

        players = getIntent().getIntExtra("players", -1);
        spys = getIntent().getIntExtra("spys", -1);

        playerNumberView = findViewById(R.id.playerTextView);
        locationView = findViewById(R.id.locationTextView);
        ((ToggleButton)findViewById(R.id.toggleShow)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    goNextPlayerFunc();
                }
                else {
                    showLocationFunc();
                }
            }
        });

        prepareFunc();
    }

    private void goNextPlayerFunc() {
        current++;
        locationView.setText("");
        if(current <= players)
            playerNumberView.setText(getResources().getString(R.string.player) + " " + String.valueOf(current));
        else {
            findViewById(R.id.toggleShow).setEnabled(false);
            findViewById(R.id.timerButton).setVisibility(View.VISIBLE);
        }
    }

    private void showLocationFunc() {
        if (!spysId.contains(new Integer(current)))
            locationView.setText(location);
        else {
            String answer = "Вы шпион!";
            if(spys != 1) {
                answer += " Ещё";
                for (int i = 0; i < spysId.size(); ++i) {
                    if (spysId.get(i) != current)
                        answer += " " +String.valueOf(spysId.get(i));
                }
                answer += "!";
            }
            locationView.setText(answer);
        }
    }

    private void prepareFunc() {
        SQLiteDatabase myDB = openOrCreateDatabase("my.db", MODE_PRIVATE, null);
        Cursor checkTable = myDB.rawQuery("select name FROM sqlite_master WHERE type='table' AND name='locations'", null);
        if(checkTable.getCount() == 0) {
            myDB.execSQL("create table locations(name text)");

            String [] defaultLocations = getResources().getStringArray(R.array.locations);
            for(int i = 0; i < defaultLocations.length; ++i) {
                ContentValues newRows = new ContentValues();
                newRows.put("name", defaultLocations[i]);
                myDB.insert("locations", null, newRows);
            }
        }
        checkTable.close();

        Cursor countLocs = myDB.rawQuery("select * from locations", null);
        int countLocations = countLocs.getCount();
        countLocs.close();

        int randNumber = (int)(Math.random() * (countLocations - 1));

        Cursor randLoc = myDB.rawQuery("select name from locations LIMIT 1 OFFSET ?", new String[] {String.valueOf(randNumber)});
        randLoc.moveToFirst();
        location = randLoc.getString(randLoc.getColumnIndex("name"));
        randLoc.close();

        myDB.close();

        while(spysId.size() < spys) {
            Integer current = new Integer ((int)(Math.random() * (players - 1)) + 1);
            if(!spysId.contains(current))
                spysId.add(current);
        }
        Collections.sort(spysId);

        playerNumberView.setText(getResources().getString(R.string.player) + " " + String.valueOf(current));
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("location", location);
        intent.putExtra("spysId", spysId);
        startActivity(intent);
    }
}
