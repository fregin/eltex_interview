package com.example.jimmy.freginapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.*;

public class LocationsActivity extends AppCompatActivity {

    private SQLiteDatabase myDB = null;
    private boolean isDefault = false;

    private SQLiteDatabase connectDB(SQLiteDatabase db) {
        if(db == null)
            return openOrCreateDatabase("my.db", MODE_PRIVATE, null);
        return db;
    }

    private SQLiteDatabase disconnectDB(SQLiteDatabase db) {
        if(db != null)
            db.close();
        return null;
    }

    private void loadDefaults() {
        if(isDefault) {
            Toast.makeText(this, "Список локаций уже сброшен!", Toast.LENGTH_SHORT).show();
            return;
        }
        myDB = connectDB(myDB);
        Cursor checkTable = myDB.rawQuery("select name FROM sqlite_master WHERE type='table' AND name='locations'", null);
        if(checkTable.getCount() == 0)
            myDB.execSQL("create table locations(name text)");

        myDB.execSQL("DELETE FROM locations");

        checkTable.close();

        String [] defaultLocations = getResources().getStringArray(R.array.locations);
        for(int i = 0; i < defaultLocations.length; ++i) {
            ContentValues newRows = new ContentValues();
            newRows.put("name", defaultLocations[i]);
            myDB.insert("locations", null, newRows);
        }
        myDB = disconnectDB(myDB);
        isDefault = true;
        Toast.makeText(this, "Список локаций сброшен!", Toast.LENGTH_SHORT).show();
    }

    private void insertLocation(String name) {
        myDB = connectDB(myDB);
        myDB.execSQL("INSERT INTO locations values('" + name + "')");
        myDB = disconnectDB(myDB);
        isDefault = false;
    }

    private void deleteLocation(String name) {
        myDB = connectDB(myDB);
        myDB.execSQL("DELETE FROM locations WHERE name='" + name + "'");
        myDB = disconnectDB(myDB);
        isDefault = false;
    }

    private LinearLayout createNewLocation(String name) {
        int sizeTv = 17;

        TextView tv = new TextView(this);
        tv.setText(name);
        tv.setTextSize(sizeTv);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        tv.setLayoutParams(tvParams);

        ImageView im = new ImageView(this);
        im.setBackgroundResource(R.drawable.location_cross);
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = (String) ((TextView)((LinearLayout) view.getParent()).getChildAt(0)).getText();
                deleteLocation(name);
                Toast.makeText(getApplicationContext(), "Локация удалена!", Toast.LENGTH_SHORT).show();
                refreshLocationsGUI();
            }
        });
        int sizeIm = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sizeTv * 1.11f, this.getResources().getDisplayMetrics());

        LinearLayout.LayoutParams imParams = new LinearLayout.LayoutParams(sizeIm, sizeIm, 0);
        imParams.gravity = Gravity.BOTTOM;
        imParams.bottomMargin = (int) (sizeIm * 0.1f);
        im.setAlpha(0.9f);
        im.setLayoutParams(imParams);
        im.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.addView(tv);
        ll.addView(im);
        return ll;
    }

    private void refreshLocationsGUI() {
        myDB = connectDB(myDB);
        Cursor checkTable = myDB.rawQuery("select name FROM sqlite_master WHERE type='table' AND name='locations'", null);
        if(checkTable.getCount() == 0)
            loadDefaults();

        LinearLayout ll = findViewById(R.id.locationsLayout);
        ll.removeAllViews();

        Cursor getLocations = myDB.rawQuery("select name FROM locations ORDER BY name", null);

        ((TextView) findViewById(R.id.countLocationsView)).setText("Количество локаций: " + String.valueOf(getLocations.getCount()));

        getLocations.moveToFirst();
        do {
            ll.addView(createNewLocation(getLocations.getString(getLocations.getColumnIndex("name"))));
        } while(getLocations.moveToNext());
        getLocations.close();
        myDB = disconnectDB(myDB);
    }

    private void addLocationsFunc() {
        String newLocation = ((EditText)findViewById(R.id.locationEditText)).getText().toString();
        if(newLocation.isEmpty()) {
            (Toast.makeText(LocationsActivity.this, "Введите локацию!", Toast.LENGTH_SHORT)).show();
            return;
        }
        myDB = connectDB(myDB);
        Cursor allLocs = myDB.rawQuery("select name from locations", null);
        allLocs.moveToFirst();
        do {
            if(allLocs.getString(allLocs.getColumnIndex("name")).equals(newLocation)) {
                (Toast.makeText(LocationsActivity.this, "Данная локация уже существует!", Toast.LENGTH_SHORT)).show();
                allLocs.close();
                myDB = disconnectDB(myDB);
                return;
            }
        } while(allLocs.moveToNext());
        allLocs.close();

        insertLocation(newLocation);
        (Toast.makeText(LocationsActivity.this, "Локация добавлена!", Toast.LENGTH_SHORT)).show();
        ((EditText) findViewById(R.id.locationEditText)).setText("");
        myDB = disconnectDB(myDB);
        refreshLocationsGUI();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        refreshLocationsGUI();
    }

    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.addLocationButton:
                addLocationsFunc();
                break;
            case R.id.restoreImageView:
                loadDefaults();
                refreshLocationsGUI();
                break;
        }
    }
}
