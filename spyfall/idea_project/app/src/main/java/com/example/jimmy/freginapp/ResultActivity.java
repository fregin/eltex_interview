package com.example.jimmy.freginapp;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ResultActivity extends AppCompatActivity {

    String location;
    ArrayList<Integer> spysId;
    Timer chrono;
    TimerTask chronoTask;
    int current = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        location = getIntent().getStringExtra("location");
        spysId = (ArrayList<Integer>) getIntent().getSerializableExtra("spysId");
        current = 7*60;
        setTimeView();

        chronoTask = new TimerTask() {
            @Override
            public void run() {
                current--;
                setTimeView();
                if(current <= 0) {
                    chrono.cancel();
                    chrono.purge();
                }
            }
        };

        chrono = new Timer();
        chrono.schedule(chronoTask, 1000, 1000);
    }

    public void onClick(View view) {
        chrono.cancel();
        chrono.purge();
        String result = location;
        if(spysId.size() > 1)
            result += "\nШпионы: ";
        else
            result += "\nШпион: ";
        result += spysId.toString().substring(1, spysId.toString().length() - 1);
        ((TextView)findViewById(R.id.resultTextView)).setText(result);
    }

    protected void setTimeView() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                String m, s;
                if(current / 60 < 10)
                    m = "0" + current / 60;
                else m = "" + current / 60;
                if(current % 60 < 10)
                    s = "0" + current % 60;
                else s = "" + current % 60;
                ((TextView)findViewById(R.id.chronometr)).setText(m + ":" + s);
            }
        }.execute();
    }
}
