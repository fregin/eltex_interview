package com.example.jimmy.freginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    protected int players = -1, spys = -1;
    RadioButton[] spysButtons = new RadioButton[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spysButtons[0] = findViewById(R.id.spysRadioButton1);
        spysButtons[1] = findViewById(R.id.spysRadioButton2);
        spysButtons[2] = findViewById(R.id.spysRadioButton3);
        spysButtons[3] = findViewById(R.id.spysRadioButton4);

        for(int i = 0; i < spysButtons.length; ++i)
            spysButtons[i].setEnabled(false);

        ((RadioGroup)findViewById(R.id.playersRadioButton)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if(id == View.NO_ID)
                    return;
                String buttonId = getResources().getResourceName(id);
                players = buttonId.charAt(buttonId.length() - 1) - '0';

                for(int i = 0; i < spysButtons.length; ++i) {
                    if(i <= players/2 - 1)
                        spysButtons[i].setEnabled(true);
                    else
                        spysButtons[i].setEnabled(false);
                }
                ((RadioGroup)findViewById(R.id.spysRadioButton)).clearCheck();
                spys = -1;
            }
        });

        ((RadioGroup)findViewById(R.id.spysRadioButton)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == View.NO_ID)
                    return;
                String buttonId = getResources().getResourceName(i);
                spys = buttonId.charAt(buttonId.length() - 1) - '0';
            }
        });
    }

    private void gameStartFunc() {
        if(players == -1)
            (Toast.makeText(MainActivity.this, "Выберите количество игроков!", Toast.LENGTH_SHORT)).show();
        else {
            if (spys == -1)
                (Toast.makeText(MainActivity.this, "Выберите количество шпионов!", Toast.LENGTH_SHORT)).show();
            else {
                Intent intent = new Intent(this, StartActivity.class);
                intent.putExtra("players", players);
                intent.putExtra("spys", spys);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        }
    }

    private void locationsViewFunc() {
        Intent intent = new Intent(this, LocationsActivity.class);
        startActivity(intent);
    }

    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.startButton: {
                gameStartFunc();
                break;
            }
            case R.id.locationsButton: {
                locationsViewFunc();
                break;
            }
        }
    }
}
