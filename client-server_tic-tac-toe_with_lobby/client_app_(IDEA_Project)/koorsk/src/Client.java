import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    static ClientWindow cWin;
    static ClientThread cTh;
    static MenuWindow mWin;
    static GameWindow gWin;
    static ViewWindow vWin;
    static String name;
    static ArrayList<InvitedWindow> inviteList = new ArrayList<>();
    public static void main(String[] args)
    {
        mWin = new MenuWindow();
    } //main
    static void Connect()   //метод подключения к серверу
    {
        cTh = new ClientThread();
        new Message(0, name.getBytes()).Send(cTh.getStream());
    }
    static void DisposeAllInvWin()  //метод закрытия всех приглашений
    {
        for(int i=0; i<inviteList.size(); i++)  //проходим список приглашений
            inviteList.get(i).dispose();    //закрываем приглашение
       inviteList.clear();  //чистим список
    }
}
class ClientThread extends Thread
{
    private boolean flag = true;
    private Socket sock;
    private BufferedInputStream bufI;
    private BufferedOutputStream bufO;
    ClientThread()  //конструктор
    {
        try {
            sock = new Socket("localhost", 3333);   //подключение к серверу
            bufI = new BufferedInputStream(sock.getInputStream());  //инициализация
            bufO = new BufferedOutputStream(sock.getOutputStream());    //потоков
        } catch (IOException e) {
            e.printStackTrace();
        }
        start();    //запуск потока
    }
    void die()
    {
        new Message(124, "Dude, let's stay friends!".getBytes()).Send(bufO);    //метод закрытия соединения с сервером
    }
    BufferedOutputStream getStream()
    {
        return bufO;
    }
    @Override
    public void run()   //жизненный цикл потока
    {
        while(flag)
        {
            Message msg = new Message();
            msg.Receive(bufI);  //читаем сообщение
            System.out.println(msg.toString());
            if(msg.code == 0)   //если код сообщения 0 (имя занято)
            {
                Client.mWin.connectBut.setEnabled(true);
                Client.mWin.str = "Nickname is already exists!";    //выводим соответствующую информацию
                Client.mWin.repaint();
                Client.cTh = null;
                die();  //закрываем подключение
                break;
            }
            if(msg.code == 1)   //если код сообщения 1 (имя свободно)
            {
                System.out.println("Connected!");
                Client.mWin.dispose();  //закрываем окно подлючения
                Client.cWin = new ClientWindow();   //создаем окно лобби
            }
            if(msg.code == 2)   //если код сообщения 2 (пришел список клиентов в лобби)
            {
                ClientWindow.UpdateLobby(msg.toString());   //обновляем список лобби
            }
            if(msg.code == 3)   //если код сообщения 3 (пришел список игр)
            {
                ClientWindow.UpdateInGame(msg.toString());  //обновляем список игр
            }
            if(msg.code == 4)   //если код сообщения 4 (пришло приглашение)
            {
                Client.inviteList.add(new InvitedWindow(msg.toString()));   //создаем окно приглашения
            }
            if(msg.code == 5)   //если код сообщения 5 (клиент отклонил приглашение)
            {
                Client.cWin.lastInvTime -= Client.cWin.delay;   //убираем задержку отсылания приглашения (анти-спам)
                System.out.println(msg.toString());
            }
            if(msg.code == 6)   //если код сообщения 6 (клиент принял приглашение)
            {
                Client.cWin.setVisible(false);  //прячем окно лобби
                if(Client.gWin!=null)   //закрываем
                     Client.gWin.dispose();
                if(Client.vWin!=null)   //лишние
                    Client.vWin.die();
                Client.DisposeAllInvWin(); //окна
                Client.gWin = new GameWindow(msg.toString());   //создаем игровое окно
            }
            if(msg.code == 7)   //если код сообщения 7 (обновление игрового поля)
            {
                Client.gWin.UpdateGame(msg.toString()); //обновляем игровое поле
            }
            if(msg.code == 8)   //если код сообщения 8 (ход клиента)
            {
                Client.gWin.yourTurn("Your turn!");
            }
            if(msg.code == 9)   //если код сообщения 9 (ход оппонента)
            {
                Client.gWin.endTurn("Enemy turn!");
            }
            if(msg.code == 10)  //если код сообщения 10 (ход клиента, но поле не пустое)
            {
                Client.gWin.yourTurn("Choose another!");
            }
            if(msg.code>=11&& msg.code<=15) //если код сообщения 11-15 (игра закончена)
            {
                Client.gWin.endTurn("Game is over!");
                JButton b = Client.gWin.but;
                b.setText("Exit!");
                b.removeActionListener(Client.gWin.but1);   //меняем слушателя
                b.addActionListener(Client.gWin.but3);  //кнопки
                b.setEnabled(true);
                new ResultWindow(msg.code); //вывод результирующего окна
            }
            if(msg.code == 16)  //если код сообщения 16 (обновления окна зрителя)
            {
                Client.vWin.UpdateGame(msg.toString());
            }
            if(msg.code == 17)  //если код сообщения 17 (оппонент готов)
            {
                Client.gWin.enemyReady(msg.toString());
            }
            if(msg.code == 18)  //если код сообщения 18 (обновление чата)
            {
                Client.cWin.chatArea.setText(Client.cWin.chatArea.getText()+msg.toString()+"\n");
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class ClientWindow extends JFrame
{
    private int windowW = 600;
    private int windowH = 400;
    private static Image img;
    long lastInvTime, delay = 3000;
    JTextField writeField;
    JScrollPane jsp;
    JTextArea chatArea;
    private static JList<String> lobbyList, ingameList;
    private JButton lobbyBut, ingameBut, exitBut, sendBut;
    ClientWindow()  //конструктор
    {
        super("");
        lobbyList = new JList<>();  //создание
        ingameList = new JList<>(); //необходимых
        lobbyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ingameList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lobbyBut = new JButton("Invite");   //элементов
        lobbyBut.setEnabled(false);
        ingameBut = new JButton("Watch");   //окна
        ingameBut.setEnabled(false);
        exitBut = new JButton("Exit");  //лобби
        chatArea = new JTextArea();
        chatArea.setEditable(false);
        jsp = new JScrollPane(chatArea);
        writeField = new JTextField();
        sendBut = new JButton("Send");
        try {
            img = ImageIO.read(getClass().getResource("back.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //настройки окна
        setResizable(false);
        setSize(windowW, windowH);
        setLocationRelativeTo(null);
        setContentPane(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setFont(new Font("Times New Roman", Font.BOLD,16));
                g.drawString(Client.name, 435,40);
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Client.cTh.die();
            }
        });
        lobbyBut.addActionListener(e -> {
            if(System.currentTimeMillis()-lastInvTime >= delay)
            {
                new Message(2, lobbyList.getSelectedValue().getBytes()).Send(Client.cTh.getStream());
                lastInvTime = System.currentTimeMillis();
            }
            else System.out.println("STOP SPAMMING!");
        });
        ingameBut.addActionListener(e -> {
            if(Client.vWin!=null)
                Client.vWin.die();
            Client.vWin = new ViewWindow(ingameList.getSelectedIndex(), ingameList.getSelectedValue());
            new Message(9, Message.IntToByte(ingameList.getSelectedIndex())).Send(Client.cTh.getStream());
        });
        sendBut.addActionListener(e -> {
            if(!writeField.getText().equals(""))
            {
                new Message(15,writeField.getText().getBytes()).Send(Client.cTh.getStream());
                writeField.setText("");
            }
        });
        exitBut.addActionListener(e -> {
            Client.cTh.die();
            dispose();
        });
        lobbyList.addListSelectionListener(e -> {
            if(lobbyList.getSelectedIndex()==-1)
                lobbyBut.setEnabled(false);
            else
                lobbyBut.setEnabled(true);
        });
        ingameList.addListSelectionListener(e -> {
            if(ingameList.getSelectedIndex()==-1)
                ingameBut.setEnabled(false);
            else
                ingameBut.setEnabled(true);
        });
        lobbyList.setBounds(80,80,130,200);
        ingameList.setBounds(225,80, 130,200);
        lobbyBut.setBounds(95,290,100,25);
        ingameBut.setBounds(240,290,100,25);
        jsp.setBounds(370,80,150,200);
        writeField.setBounds(370,290,150,25);
        sendBut.setBounds(520,290,70,25);
        exitBut.setBounds(450,330,100,25);
        setLayout(null);
        add(lobbyList);
        add(ingameList);
        add(lobbyBut);
        add(ingameBut);
        add(jsp);
        add(writeField);
        add(sendBut);
        add(exitBut);
        setVisible(true);
    }
    static void UpdateLobby(String s)   //метод обновления списка лобби
    {
        String[] ss = s.split(" ");
        ArrayList<String> temp = new ArrayList<>(); //временный список
        for(int i=0; i<ss.length;i++)
            temp.add(ss[i]);    //заполняем список
        if(temp.contains(Client.name))  //если находим свое имя
            temp.remove(Client.name);   //удаляем из списка
        if(temp.size()!=0)
        {
            String [] t = new String[temp.size()];  //из списка делаем массив строк
            t = temp.toArray(t);
            lobbyList.setListData(t);   //обновляем список
        }
        else lobbyList.setListData(new String[0]);  //если список пустой (или в лобби только мы)
        lobbyList.repaint();
    }
    static void UpdateInGame(String s)  //метод обновления списка игр
    {
        if(s.equals(""))    //если список пуст
            ingameList.setListData(new String[0]);
        else
        {
            String[] ss = s.split(" ");
            for (int i = 0; i < ss.length; i++) //заполняем массив названий игр
                ss[i] = ss[i].replaceAll("[-]", " vs ");
            ingameList.setListData(ss); //обновляем список
        }
        ingameList.repaint();
    }
}
class MenuWindow extends JFrame
{
    private int windowW = 600;
    private int windowH = 400;
    private JTextField jt;
    String str = "";
    JButton connectBut;
    private static Image img;
    private static ImageIcon conIcon;
    MenuWindow()    //конструктор аналогично создание элементов окна, настройка окна и добавление элементов
    {
        super("");
        try {
            img = ImageIO.read(getClass().getResource("menu.png"));
            conIcon = new ImageIcon(ImageIO.read(getClass().getResource("conbut.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        jt = new JTextField();
        jt.setFont(new Font("Times New Roman", Font.BOLD,16));
        connectBut = new JButton();
        connectBut.setIcon(conIcon);
        connectBut.setEnabled(false);
        connectBut.addActionListener(e -> {
            if(jt.getText().isEmpty())
                return;
            Client.name = jt.getText();
            Client.Connect();
            connectBut.setEnabled(false);
        });
        AbstractDocument doc = (AbstractDocument) jt.getDocument();
        doc.setDocumentFilter(new LengthFilter(0, 19));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(windowW, windowH);
        setLocationRelativeTo(null);
        setContentPane(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setColor(Color.red);
                g.drawString(str, 182, 108);
            }
        });
        jt.setBorder(BorderFactory.createLineBorder(Color.red));
        jt.setBounds(182,66,150,25);
        connectBut.setBounds(362,66,120,25);
        setLayout(null);
        add(jt);
        add(connectBut);
        setVisible(true);
    }
}
class InvitedWindow extends JFrame
{
    private int windowW = 250;
    private int windowH = 150;
    private static Image img;
    private JButton accept, decline;
    private String invitedName;
    InvitedWindow(String inv) {
        super("");
        invitedName = inv;
        accept = new JButton("Accept");
        decline = new JButton("Decline");
        try {
            img = ImageIO.read(getClass().getResource("invback.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setResizable(false);
        setSize(windowW, windowH);
        setLocationRelativeTo(null);
        setContentPane(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setFont(new Font("Times New Roman", Font.BOLD, 16));
                g.drawString(invitedName, 35, 40);
                g.drawString("invited you to play!", 35, 55);
            }
        });
        accept.addActionListener(e -> {
            new Message(3,invitedName.getBytes()).Send(Client.cTh.getStream());
            Client.inviteList.remove(this);
            dispose();
        });
        decline.addActionListener(e -> {
            die();
            dispose();
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Message(4,invitedName.getBytes()).Send(Client.cTh.getStream());
                Client.inviteList.remove(this);
        }
        });
        accept.setBounds(24,90,100,25);
        decline.setBounds(126,90,100,25);
        setLayout(null);
        add(accept);
        add(decline);
        setVisible(true);
    }
    void die()
    {
        new Message(4,invitedName.getBytes()).Send(Client.cTh.getStream());
        Client.inviteList.remove(this);
        dispose();
    }
}
class GameWindow extends JFrame
{
    private int windowW = 600;
    private int windowH = 400;
    private ImageIcon[] icons;
    private String enemy, status="Waiting for readyness!";
    private Image img;
    private JPanel jp1;
    private JLabel[] jl;
    JButton but;
    ActionListener but1, but2, but3;
    private MyMouseAdapter[] aList;
    GameWindow(String en)   //конструктор
    {
        super("");
        enemy = en;
        setSize(windowW,windowH); //настройка окна
        setResizable(false);
        setLocationRelativeTo(null);
        try {   //загрузка иконок для игрового поля
            icons = new ImageIcon[3];
            icons[0] = new ImageIcon(ImageIO.read(getClass().getResource("empty.png")));
            icons[1] = new ImageIcon(ImageIO.read(getClass().getResource("krest.png")));
            icons[2] = new ImageIcon(ImageIO.read(getClass().getResource("nol.png")));
            img = ImageIO.read(getClass().getResource("gameback.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setContentPane(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setFont(new Font("Times New Roman", Font.BOLD,16));
                g.drawString(enemy, 180,43);
                g.drawString(status, 180,75);
            }
        });
        jp1 = new JPanel();
        jp1.setBounds(230,120,140,140);
        jp1.setLayout(new GridLayout(3,3));
        jl = new JLabel[9]; //создание игрового поля
        aList = new MyMouseAdapter[9];  //добавление слушателей
        for(int i=0; i<jl.length; i++)
        {
            jl[i] = new JLabel(icons[0]);
            aList[i] = new MyMouseAdapter(i);
            jp1.add(jl[i]); //добавление элементов в окно
        }
        but = new JButton("Ready!!");
        but.setBounds(250,290,100,25);
        but1 = e -> {
            new Message(6, "Ready!".getBytes()).Send(Client.cTh.getStream());
            but.setText("Waiting");
            but.setEnabled(false);
            but.removeActionListener(but1);
            but.addActionListener(but2);
        };
        but2 = e -> {
            new Message(8, "Surrender!".getBytes()).Send(Client.cTh.getStream());
            Client.cWin.setVisible(true);
            dispose();
        };
        but3 = e -> {
            Client.cWin.setVisible(true);
            dispose();
        };
        but.addActionListener(but1);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Message(8, "Surrender!".getBytes()).Send(Client.cTh.getStream());
                Client.cWin.setVisible(true);
            }
        });
        add(but);
        setLayout(null);
        add(jp1);
        setVisible(true);
    }
    void yourTurn(String s) //метод включения слушателей игрового поля
    {
        status = s;
        for(int i=0; i<jl.length; i++)
            jl[i].addMouseListener(aList[i]);
        repaint();
    }
    void endTurn(String s)  //метод выключения слушателей игрового поля
    {
        status = s;
        for(int i=0; i<jl.length; i++)
            jl[i].removeMouseListener(aList[i]);
        repaint();
    }
    void UpdateGame(String s)   //метод обновления игрового окна
    {
        if(!but.isEnabled())    //если началась игра
        {
            but.setText("Surrender");   //обновляем нашу кнопку
            but.setEnabled(true);
        }
        char [] c = s.toCharArray();
        for(int i=0; i<c.length; i++)   //обновляем игровое поле
            jl[i].setIcon(icons[c[i]-'0']);
    }
    void enemyReady(String s)
    {
        status = s+" is ready!";
        repaint();
    }
}
class ViewWindow extends JFrame
{
    int windowW = 600;
    int windowH = 400;
    ImageIcon[] icons;
    String name;
    Image img;
    JPanel jp1;
    JLabel[] jl;
    int index;
    ViewWindow(int ind, String n)
    {
        super("");
        index = ind;
        name = n;
        setSize(windowW,windowH);
        setResizable(false);
        setLocationRelativeTo(null);
        try {
            icons = new ImageIcon[3];
            icons[0] = new ImageIcon(ImageIO.read(getClass().getResource("empty.png")));
            icons[1] = new ImageIcon(ImageIO.read(getClass().getResource("krest.png")));
            icons[2] = new ImageIcon(ImageIO.read(getClass().getResource("nol.png")));
            img = ImageIO.read(getClass().getResource("viewback.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        jp1 = new JPanel();
        jp1.setBounds(230,120,140,140);
        jp1.setLayout(new GridLayout(3,3));
        jl = new JLabel[9];
        for(int i=0; i<jl.length; i++)
        {
            jl[i] = new JLabel(icons[0]);
            jp1.add(jl[i]);
        }
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Message(10, Message.IntToByte(index)).Send(Client.cTh.getStream());
            }
        });
        setContentPane(new JComponent()
        {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setFont(new Font("Times New Roman", Font.BOLD,16));
                g.drawString(name, 30,45);
            }
        });
        setLayout(null);
        add(jp1);
        setVisible(true);
    }
    void die()
    {
        new Message(10, Message.IntToByte(index)).Send(Client.cTh.getStream());
        dispose();
    }
    void UpdateGame(String s)
    {
        char [] c = s.toCharArray();
        for(int i=0; i<c.length; i++)
            jl[i].setIcon(icons[c[i]-'0']);
    }
}
class ResultWindow extends JFrame
{
    int windowW = 300;
    int windowH = 200;
    String str;
    Image img;
    JButton but;
    ResultWindow(int type)
    {
        super("");
        setSize(windowW,windowH);
        setResizable(false);
        setLocationRelativeTo(null);
        String s = null;
        if(type==11)
        {
            s = "draw.png";
            str = "Draw!";
        }
        if(type==12)
        {
            s = "win.png";
            str = "You won!";
        }
        if(type==13)
        {
            s = "lose.png";
            str = "You lost!";
        }
        if(type==14)
        {
            s = "win.png";
            str = "Opponent left!";
        }
        if(type==15)
        {
            s = "win.png";
            str = "Opponent surrended!";
        }
        try {
            img = ImageIO.read(getClass().getResource(s));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setContentPane(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(img, 0, 0, this);
                g.setFont(new Font("Times New Roman", Font.BOLD,16));
                g.drawString(str,150,25);
            }
        });
        but = new JButton("Got it!");
        but.addActionListener(e ->  {
            dispose();
        });
        but.setBounds(170,130,100,25);
        setLayout(null);
        add(but);
        setVisible(true);
    }
}
class MyMouseAdapter extends MouseAdapter
{
    int ind;
    MyMouseAdapter(int i)
    {
        super();
        ind = i;
    }
    public void mouseClicked (MouseEvent e) {
        new Message(7,Integer.toString(ind).getBytes()).Send(Client.cTh.getStream());
        Client.gWin.endTurn("Enemy turn!");
    }
}
class LengthFilter extends DocumentFilter
{
    private int currentLength;
    private int maxLength;
    public LengthFilter(int currentLength, int maxLength)
    {
        this.currentLength = currentLength;
        this.maxLength = maxLength;
    }
    public void remove(FilterBypass fb, int offset, int length)
    {
        currentLength -= length;
        try {
            fb.remove(offset, length);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        if(currentLength == 0)
            Client.mWin.connectBut.setEnabled(false);
    }
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
    {
        string = string.replaceAll("[  \\p{InCyrillic}]", "");
        if((currentLength + string.length()) <= maxLength)
        {
            currentLength += string.length();
            try {
                fb.insertString(offset, string, attr);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        if(currentLength != 0)
            Client.mWin.connectBut.setEnabled(true);
    }
    public void replace(FilterBypass fb, int offset, int length, String string, AttributeSet attr)
    {
        string = string.replaceAll("[  \\p{InCyrillic}]", "");
        if((currentLength - length + string.length()) <= maxLength)
        {
            currentLength += string.length() - length;
            try {
                fb.replace(offset, length, string, attr);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        if(currentLength != 0)
            Client.mWin.connectBut.setEnabled(true);
        else Client.mWin.connectBut.setEnabled(false);
    }
}
class Message
{
    int code;
    byte[] data;
    Message()   {}
    Message(int c, byte[] b)
    {
        code = c;
        data = b.clone();
    }
    public String toString()
    {
        return new String(data);
    }
    public static byte[] IntToByte(int v)
    {
        return new byte[] {
                (byte)(v >> 24),
                (byte)(v >> 16),
                (byte)(v >> 8),
                (byte) v};
    }
    public static int ByteToInt(byte[] b)
    {
        return ((b[0] & 0xFF) << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8) + (b[3] & 0xFF);
    }
    void Send(BufferedOutputStream o)
    {
        try {
            o.write(IntToByte(code));
            o.write(IntToByte(data.length));
            o.write(data);
            o.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void Receive(BufferedInputStream i)
    {
        byte[] c = new byte[4];
        byte[] l = new byte[4];
        try {
            int k=0;
            while(k < 4)
            {
                i.read(c, k,1);
                k++;
            }
            k=0;
            while(k < 4)
            {
                i.read(l, k,1);
                k++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int len = ByteToInt(l);
        data = new byte[len];
        try {
            int k=0;
            while(k < len)
            {
                i.read(data, k,1);
                k++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        code = ByteToInt(c);
    }
}