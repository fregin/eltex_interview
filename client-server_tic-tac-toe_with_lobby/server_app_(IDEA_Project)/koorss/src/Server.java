import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    static ArrayList<ServerThread> sTh = new ArrayList<>();
    static ArrayList<GameThread> gTh = new ArrayList<>();
    static InfoThread in;
    public static void main(String[] args) //main
    {
        try {
            ServerSocket sS = new ServerSocket(3333);   //создание серверного сокета
            System.out.println("Server has started!");
            in = new InfoThread();  //запуск потока рассылки информации
            while(true) //бесконечный цикл
            {
                Socket s = sS.accept(); //ожидание подключения нового клиента
                sTh.add(new ServerThread(s, sTh.size()));   //создание потока чтения и добавления его в список
                in.isChanged = true;    //оповещаем поток рассылки, что количество клиентов изменилось
                System.out.println("New client connected!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static int Search(String s) //вспомогательный метод для получения индекса потока чтения по имени клиента
    {
        for (int i = 0; i < sTh.size(); i++) {  //проходим весь список
            if (sTh.get(i).name.equals(s))  //если клиент найден
                return i;   //возвращаем индекс клиента
        }
        return -1;  //если клиента нет,возвращаем -1
    }
    static GameThread addGameThread(ServerThread t1, ServerThread t2)   //метод, добавляющий поток новой игры в список
    {
        GameThread g = new GameThread(t1,t2);   //создаем новый поток
        gTh.add(g); //добавляем в список
        return g;   //возвращаем ссылку на созданный игровой поток
    }
}
class InfoThread extends Thread
{
    boolean flag = true, isChanged = false;
    String lobby, ingame, chat="";
    InfoThread()
    {
        start();
    }   //конструктор
    void RefreshLobby() //метод рассылки списка подключенных к лобби клиентов
    {
        lobby = ""; //опустошаем результирующую строку
        for(int i=0; i<Server.sTh.size(); i++) //проходим по всему списку клиентов
        {
            if(Server.sTh.get(i).status == 0)   //если статус клиента ==0 (находится в лобби)
            {
                if(!lobby.equals(""))
                    lobby += " ";   //ставим пробел между именами клиентов
                lobby += Server.sTh.get(i).name;    //записываем очередное имя
            }
        }
        for(int i=0; i<Server.sTh.size(); i++)  //всем клиентам
            new Message(2, lobby.getBytes()).Send(Server.sTh.get(i).getStream());   //отправляем список клиентов в лобби
    }
    void RefreshInGame()    //метод рассылки списка текущих игр
    {
        ingame = "";    //опустошаем результирующую строку
        for(int i=0; i<Server.gTh.size(); i++)  //проходим по всему списку игровых потоков
        {
            if(!ingame.equals(""))
                ingame += " ";  //ставим пробел между названиями игр
            ingame += Server.gTh.get(i).p1.name + "-" + Server.gTh.get(i).p2.name; //записываем очередное имя игры
        }
        for(int i=0; i<Server.sTh.size(); i++)  //всем клиентам
            new Message(3, ingame.getBytes()).Send(Server.sTh.get(i).getStream());  //отправляем список текущих игр
    }
    private boolean RefreshSockets()    //метод для проверки отключенных клиентов
    {
        boolean isChanged = false;  //присваиваем результат работы метода false
        for(int i = 0; i < Server.sTh.size(); i++)  //проходим список всех клиентов
        {
            ServerThread serv = Server.sTh.get(i);
            if (serv.getSocket().isClosed()) {  //если сокет закрыт
                if (!isChanged)
                    isChanged = true;   //результат становится true
                if (serv.game!=null)
                    serv.game.LostConnection(serv.name);    //если данный клиент числится в игре, то записываем ему поражение
                Server.sTh.remove(i--); //удаляем клиента из списка
            }
        }
        return isChanged;   //возвращаем результат работы метода
    }
    void RefreshChat()  //метод обновления чата
    {
        for(int i=0; i<Server.sTh.size(); i++)  //проходим по всем клиентам
            new Message(18,chat.getBytes()).Send(Server.sTh.get(i).getStream());    //отправляем новое сообщение
        chat = "";
    }
    @Override
    public void run()   //жизненный цикл потока
    {
        while(flag)
        {
            if(RefreshSockets() || isChanged)   //если клиент отключился или подключился
            {
                isChanged = false;
                RefreshLobby(); //обновляем
                RefreshInGame(); //списки
            }
            if(!chat.equals(""))    //если кто-то отправил новое сообщение в чат
            {
                RefreshChat();  //обновляем чат
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class ServerThread extends Thread
{
    boolean flag = true;
    int status = 0;
    GameThread game;
    int ind;
    String name;
    private Socket sock;
    private BufferedInputStream bufI;
    private BufferedOutputStream bufO;
    ServerThread(Socket s, int index)   //конструктор
    {
        try {
            ind = index;    //запись индекса клиента
            sock = s;   //запись сокета
            bufI = new BufferedInputStream(s.getInputStream()); //инициализация
            bufO = new BufferedOutputStream(s.getOutputStream());   //потоков
            start();    //запуск потока
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    BufferedOutputStream getStream()
    {
        return bufO;
    }   //метод, возращающий выходной поток
    Socket getSocket()
    {
        return sock;
    }   //метод, возварщающий сокет
    @Override
    public void run()   //жизненный цикл потока
    {
        while(flag)
        {
            Message msg = new Message();
            msg.Receive(bufI);  //получаем сообщение
            System.out.println(msg.toString());
            if(msg.code == 0)   //если код сообщения 0 (подлючение клиента к серверу
            {
                boolean isBusy = false;
                name = msg.toString();  //записываем имя клиента
                for(int i=0; i<Server.sTh.size(); i++)  //цикл проверки уникальности имени
                {
                    if(i == ind)
                        continue;
                    if(name.equals(Server.sTh.get(i).name))
                    {
                        isBusy = true;
                        break;
                    }
                }
                if(isBusy)  //если имя занято
                    new Message(0, "The name is busy!".getBytes()).Send(bufO);  //отсылаем соответствующее сообщение
                else    //иначе
                    new Message(1, "Good!".getBytes()).Send(bufO);  //отсылаем соответствующее сообщение
            }
            if(msg.code == 2)   //если код сообщения 2 (клиент отправляет приглашение другому)
            {
                if(Server.Search(msg.toString())<Server.sTh.size()) //проверяем существует ли клиент с таким индексом
                    new Message(4, name.getBytes()).Send(Server.sTh.get(Server.Search(msg.toString())).getStream());    //отправляем этому клиенту соответсвующее сообщение
            }
            if(msg.code == 3)   //если код сообщения 3 (клиент принял приглашение)
            {
                if(Server.Search(msg.toString())<Server.sTh.size()) //проверяем существует ли клиент с таким индексом
                {
                    ServerThread op = Server.sTh.get(Server.Search(msg.toString()));
                    if(status == 0 && op.status == 0)   //оба клиента находятся в лобби
                    {
                        game = Server.addGameThread(this, op);  //создаем игру
                        Server.in.isChanged = true; //говорим потоку рассылки, что необходимо обновить списки
                    }
                }
            }
            if(msg.code == 4)    //если код сообщения 4 (клиент отклонил приглашение)
            {
                if(Server.Search(msg.toString())<Server.sTh.size()) //проверяем существует ли клиент с таким индексом
                    new Message(5, name.getBytes()).Send(Server.sTh.get(Server.Search(msg.toString())).getStream());    //отсылаем соответствующее сообщение
            }
            if(msg.code == 6)   //если код сообщения 6 (клиент подтверждает свою готовность)
            {
                if(game!=null)  //если игрок в игре
                    game.Ready(name);   //отсылаем игровому потоку готовность
            }
            if(msg.code == 7)   //если код сообщения 7 (клиент отсылает свой ход)
            {
                if(game!=null)  //если игрок в игре
                    game.Update(name, Integer.parseInt(msg.toString()));    //обновляем игровое поле
            }
            if(msg.code == 8)   //если код сообщения 8 (клиент осдается)
            {
                if(game!=null)  //если игрок в игре
                    game.Surrender(name);   //игрок сдается
            }
            if(msg.code == 9)   //если код сообщения 9(клиент желает смотреть за игрой)
            {
                if(Message.ByteToInt(msg.data)<Server.gTh.size())   //проверяем существует ли требуемая игра
                {
                    GameThread g = Server.gTh.get(Message.ByteToInt(msg.data));
                    g.addViewer(this);  //добавляем зрителя в игровой поток
                    new Message(16, g.gameStatus.getBytes()).Send(getStream()); //отсылаем зрителю текущее состояние игры
                }
            }
            if(msg.code == 10)  //если код сообщения 10(клиент перестал смотреть за игрой)
            {
                if(Message.ByteToInt(msg.data)<Server.gTh.size())   //проверяем существует ли требуемая игра
                    Server.gTh.get(Message.ByteToInt(msg.data)).removeViewer(this); //удаляем зрителя
            }
            if(msg.code == 15)  //если код сообщения 15(клиент написал новое сообщение в чат)
            {
                Server.in.chat = name+": "+msg.toString();
            }
            if(msg.code == 124) //если код сообщения 124(клиент отключается)
            {
                flag = false;   //прекратить жизненный цикл потока
                new Message(124,"You are killed!".getBytes()).Send(bufO);   //отправить соответствующее сообщение
                try {
                    bufO.close();   //закрываем
                    bufI.close();   //потоки
                    sock.close();   //и сокет
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class GameThread extends Thread
{
    boolean isReady1 = false, isReady2 = false, isStarted = false;
    String gameStatus = "000000000";
    char c1, c2;
    int turn;
    ArrayList<ServerThread> viewers = new ArrayList<>();
    ServerThread p1, p2;
    GameThread(ServerThread pp1, ServerThread pp2)  //конструктор
    {
        p1 = pp1;   //запись
        p2 = pp2;   //клиентов
        p1.status = 1;  //изменение
        p2.status = 1;  //статусов
        p1.game = this; //запись ссылки
        p2.game = this; //на игровой поток
        new Message(6,p2.name.getBytes()).Send(p1.getStream()); //отсылаем имя оппонента
        new Message(6,p1.name.getBytes()).Send(p2.getStream()); //каждому клиенту игры
        start();    //запускаем поток
    }
    void addViewer(ServerThread p)  //метод, добавляющий зрителя
    {
        if(!viewers.contains(p))    //если уже не смотрит
            viewers.add(p); //добавляем зрителя
    }
    void removeViewer(ServerThread p)   //метод, удаляющий зрителя
    {
        if(viewers.contains(p)) //если такой зритель есть
            viewers.remove(p);  //удаляем зрителя
    }
    void Ready(String p)    //метод, подтверждения готовности
    {
        if(p.equals(p1.name))   //если это первый клиент
        {
            isReady1 = true;    //готовность первого клиента
            new Message(17, p1.name.getBytes()).Send(p2.getStream());   //отсылаем соответствующее сообщение
        }
        else    //иначе
        {
            isReady2 = true;    //готовность второго клиента
            new Message(17, p2.name.getBytes()).Send(p1.getStream());   //отсылаем соответствующее сообщение
        }
    }
    @Override
    public void run()   //жизненный цикл потока
    {
        while(!isStarted)
        {
            if(isReady1&&isReady2&&!isStarted)  //если клиенты готовы и игра не начата
            {
                isStarted = true;
                new Message(7, gameStatus.getBytes()).Send(p1.getStream()); //отсылаем игровое поле
                new Message(7, gameStatus.getBytes()).Send(p2.getStream()); //игрокам
                for(int j=0; j<viewers.size(); j++)
                    new Message(16, gameStatus.getBytes()).Send(viewers.get(j).getStream());    //зрителям
                if(Math.random()<0.5)   //определение того, кто будет ходить первым
                {
                    c1 = '1';
                    c2 = '2';
                    turn = 1;   //первый клиент
                }
                else
                {
                    c1 = '2';
                    c2 = '1';
                    turn = 2;   //второй клиент
                }
                if(turn%2==1)   //если первый клиент ходит первый
                {
                    new Message(8, "Your turn!".getBytes()).Send(p1.getStream());   //отправляем
                    new Message(9, "Enemy turn!".getBytes()).Send(p2.getStream());  //соответствующие сообщения
                }
                else    //иначе
                {
                    new Message(8, "Your turn!".getBytes()).Send(p2.getStream());   //отправляем
                    new Message(9, "Enemy turn!".getBytes()).Send(p1.getStream());  //соответствующие сообщения
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void Update(String p, int i) //метод обновления игрового поля
    {
        if(gameStatus.toCharArray()[i]!='0')    //если поле уже занято
        {
            new Message(10,"Chose another, dude!".getBytes()).Send(Server.sTh.get(Server.Search(p)).getStream());   //отправляем соответствующее сообщение
            return;
        }
        if(p.equals(p1.name))   //если прислал сообщение первый игров
        {
            if(turn%2==0)   //но это не его ход
            {
                new Message(20, "That's not your turn!".getBytes()).Send(p1.getStream());   //отправляем соответствующее сообщение
                return;
            }
            gameStatus = gameStatus.substring(0,i)+c1+gameStatus.substring(i+1,9);  //иначе изменяем игровое поле
        }
        else    //аналогично со вторым игроков
        {
            if(turn%2==1)
            {
                new Message(20, "That's not your turn!".getBytes()).Send(p2.getStream());
                return;
            }
            gameStatus = gameStatus.substring(0,i)+c2+gameStatus.substring(i+1,9);
        }
        if(!checkWin()) //проверка конца игры
            changeTurn();   //конец хода
        new Message(7, gameStatus.getBytes()).Send(p1.getStream()); //рассылка игрового поля
        new Message(7, gameStatus.getBytes()).Send(p2.getStream()); //игрокам
        for(int j=0; j<viewers.size(); j++)
            new Message(16, gameStatus.getBytes()).Send(viewers.get(j).getStream());    //зрителям
    }
    boolean checkWin()  //метод проверки конца игры
    {
        char[] str = gameStatus.toCharArray();
        int count;
        for(int i=0; i<3; i++) //проверка строк
        {
            count = 0;
            char c = str[3*i];  //запоминаем первый символ
            if(c == '0')
                continue;
            for(int j=1; j<3; j++)  //проходим 3 строки
                if(str[3*i+j]==c)   //если символ совпадает с первым
                    count++;
            if(count == 2)  //если три символа совпадают
            {
                FinishGame(c);  //конец игры
                return true;
            }
        }
        for(int j=0; j<3; j++)  //аналогично с вертикальными линиями и диагональными
        {
            count = 0;
            char c = str[j];
            if(c == '0')
                continue;
            for(int i=1; i<3; i++)
                if(str[3*i+j]==c)
                    count++;
            if(count == 2)
            {
                FinishGame(c);
                return true;
            }
        }
        count = 0;
        char c = str[0];
        if(c != '0')
            for(int j=4; j<9; j+=4)
            {
                if(c==str[j])
                    count++;
            }
        if(count == 2)
        {
            FinishGame(c);
            return true;
        }
        count = 0;
        c = str[2];
        if(c != '0')
            for(int j=4; j<7; j+=2)
            {
                if(c==str[j])
                    count++;
            }
        if(count == 2)
        {
            FinishGame(c);
            return true;
        }
        if(gameStatus.indexOf('0')==-1) //если больше нет пустых полей
        {
            FinishGame('3');    //конец игры
            return true;
        }
        return false;
    }
    void FinishGame(char win)   //метод завершения игры (входной параметр символизирует победителя
    {
        if(win=='3')    //ничья
        {
            new Message(11, "Draw!".getBytes()).Send(p1.getStream());   //отправляем игрока
            new Message(11, "Draw!".getBytes()).Send(p2.getStream());   //соответствующее сообщения
        }
        else
        {
            if(win==c1) //если выиграл первый
            {
                new Message(12, "You win!".getBytes()).Send(p1.getStream());    //отправляем игрока
                new Message(13, "You lose!".getBytes()).Send(p2.getStream());   //соответствующее сообщения
            }
            else    //иначе
            {
                new Message(12, "You win!".getBytes()).Send(p2.getStream());    //отправляем игрока
                new Message(13, "You lose!".getBytes()).Send(p1.getStream());   //соответствующее сообщения
            }
        }
        p1.status = 0;  //восстанавливаем
        p2.status = 0;  //статус
        p1.game = null; //удаляем у клиентов ссылку
        p2.game = null; //на игру
        Server.gTh.remove(this);    //удаляем игру из списка игр
        Server.in.isChanged = true; //сообщаем потоку рассылок о необходимости обновить списки
    }
    void changeTurn()   //метод завершения хода
    {
        turn++;
        if(turn%2==1)   //если ход первого игрока
        {
            new Message(8, "Your turn!".getBytes()).Send(p1.getStream());   //отправляем игрока
            new Message(9, "Enemy turn!".getBytes()).Send(p2.getStream());  //соответствующее сообщения
        }
        else    //иначе
        {
            new Message(8, "Your turn!".getBytes()).Send(p2.getStream());   //отправляем игрока
            new Message(9, "Enemy turn!".getBytes()).Send(p1.getStream());  //соответствующее сообщения
        }
    }
    void LostConnection(String p)   //метод авто-победы в случае отключения оппонента
    {
        if(p.equals(p1.name))   //если отсоединился первый
        {
            new Message(14,"LostConnectionWIN!".getBytes()).Send(p2.getStream());   //отправляем соответствующее сообщение
            p2.status = 0;
            p2.game = null;
        }
        else    //иначе
        {
            new Message(14,"LostConnectionWIN!".getBytes()).Send(p1.getStream());   //отправляем соответствующее сообщение
            p1.status = 0;
            p1.game = null;
        }
        Server.gTh.remove(this);
        Server.in.isChanged = true;
    }
    void Surrender(String p)    //метод авто-победы в случае если оппонент сдался
    {
        if(p.equals(p1.name))   //если сдался первый
            new Message(15,"OpponentSurrended".getBytes()).Send(p2.getStream());    //отправляем соответствующее сообщение
        else    //иначе
            new Message(15,"OpponentSurrended".getBytes()).Send(p1.getStream());    //отправляем соответствующее сообщение
        p1.status = 0;
        p2.status = 0;
        p1.game = null;
        p2.game = null;
        Server.gTh.remove(this);
        Server.in.isChanged = true;
    }
}
class Message
{
    int code;
    byte[] data;
    Message()   {}
    Message(int c, byte[] b)
    {
        code = c;
        data = b.clone();
    }
    public String toString()
    {
        return new String(data);
    }
    public static byte[] IntToByte(int v)
    {
        return new byte[] {
                (byte)(v >> 24),
                (byte)(v >> 16),
                (byte)(v >> 8),
                (byte) v};
    }
    public static int ByteToInt(byte[] b)
    {
        return ((b[0] & 0xFF) << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8) + (b[3] & 0xFF);
    }
    void Send(BufferedOutputStream o)
    {
        try {
            o.write(IntToByte(code));
            o.write(IntToByte(data.length));
            o.write(data);
            o.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void Receive(BufferedInputStream i)
    {
        byte[] c = new byte[4];
        byte[] l = new byte[4];
        try {
            int k=0;
            while(k < 4)
            {
                i.read(c, k,1);
                k++;
            }
            k=0;
            while(k < 4)
            {
                i.read(l, k,1);
                k++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int len = ByteToInt(l);
        data = new byte[len];
        try {
            int k=0;
            while(k < len)
            {
                i.read(data, k,1);
                k++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        code = ByteToInt(c);
    }
}