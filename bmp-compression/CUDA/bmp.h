#pragma once
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;

struct color
{
	unsigned char r = 0;
	unsigned char g = 0;
	unsigned char b = 0;
};

struct bitmaphead
{
	char type[2];
	int filesize;
	__int16 res[2];
	int offset;

	int size;
	int width;
	int height;
	__int16 planes;
	__int16 den;
	int typecomp;
	int sizecomp;
	int xppm;
	int yppm;
	int clrs;
	int clrimp;
} bmpheader;