#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "bmp.h"
#include <glut.h>

//���������� ����������� ����������� ����� ���������� ����� �������� (2�2, 3�3).

color *pix, *fil;
int w1, h1, w2, h2;
int group = 2;

void makepixmas(FILE* file)
{
	w1 = bmpheader.width;
	h1 = bmpheader.height;
	pix = new color [w1*h1];
	int trash = (w1*bmpheader.den / 8) % 4;
	cout << "Trash is " << trash << endl;
	for (int i = 0; i < h1 && !feof(file); i++)
	{
		for (int j = 0; j <w1 && !feof(file); j++)
		{
			pix[i*w1 + j].b = getc(file);
			pix[i*w1 + j].g = getc(file);
			pix[i*w1 + j].r = getc(file);
		}
		for (int ii = 0; ii < trash; ii++)
			getc(file);
	}
}
int readthefile(FILE* file)
{
	fread(&bmpheader.type, sizeof(char), 2, file);
	if (bmpheader.type[0] != 'B' || bmpheader.type[1] != 'M')
	{
		cout << "That's not the bmp file!" << endl;
		return 1;
	}
	fread(&bmpheader.filesize, sizeof(int), 1, file);
	fread(&bmpheader.res, sizeof(__int16), 2, file);
	fread(&bmpheader.offset, sizeof(int), 1, file);
	fread(&bmpheader.size, sizeof(int), 1, file);
	fread(&bmpheader.width, sizeof(int), 1, file);
	fread(&bmpheader.height, sizeof(int), 1, file);
	fread(&bmpheader.planes, sizeof(__int16), 1, file);
	fread(&bmpheader.den, sizeof(__int16), 1, file);
	fread(&bmpheader.typecomp, sizeof(int), 1, file);
	fread(&bmpheader.sizecomp, sizeof(int), 1, file);
	fread(&bmpheader.xppm, sizeof(int), 1, file);
	fread(&bmpheader.yppm, sizeof(int), 1, file);
	fread(&bmpheader.clrs, sizeof(int), 1, file);
	fread(&bmpheader.clrimp, sizeof(int), 1, file);
	cout << bmpheader.type[0] << bmpheader.type[1] << endl;
	cout << bmpheader.filesize << endl;
	cout << bmpheader.res[0] << bmpheader.res[1] << endl;
	cout << bmpheader.offset << endl;
	cout << bmpheader.size << endl;
	cout << bmpheader.width << endl;
	cout << bmpheader.height << endl;
	cout << bmpheader.planes << endl;
	cout << bmpheader.den << endl;
	cout << bmpheader.typecomp << endl;
	cout << bmpheader.sizecomp << endl;
	cout << bmpheader.xppm << endl;
	cout << bmpheader.yppm << endl;
	cout << bmpheader.clrs << endl;
	cout << bmpheader.clrimp << endl;
	return 0;
}
void writethefile(FILE *file)
{
	int n = 0;
	__int16 n16 = 0;
	fwrite(bmpheader.type, sizeof(char), 2, file);
	n = (w2*h2*bmpheader.den / 8 + 54);
	fwrite(&n, sizeof(int), 1, file);
	n16 = 0;
	fwrite(&n16, sizeof(__int16), 1, file);
	fwrite(&n16, sizeof(__int16), 1, file);
	n = 54;
	fwrite(&n, sizeof(int), 1, file);
	n = 40;
	fwrite(&n, sizeof(int), 1, file);
	fwrite(&w2, sizeof(int), 1, file);
	fwrite(&h2, sizeof(int), 1, file);
	n16 = 1;
	fwrite(&n16, sizeof(__int16), 1, file);
	fwrite(&bmpheader.den, sizeof(__int16), 1, file);
	n = 0;
	fwrite(&n, sizeof(int), 1, file);
	n = (w2*h2*bmpheader.den / 8);
	fwrite(&n, sizeof(int), 1, file);
	fwrite(&bmpheader.xppm, sizeof(int), 1, file);
	fwrite(&bmpheader.yppm, sizeof(int), 1, file);
	fwrite(&bmpheader.clrs, sizeof(int), 1, file);
	fwrite(&bmpheader.clrimp, sizeof(int), 1, file);
	int trash = (w2*bmpheader.den / 8) % 4;
	cout << "Trash is " << trash << endl;
	char trashchar = '&';
	for (int i = 0; i < h2 && !feof(file); i++)
	{
		for (int j = 0; j <w2 && !feof(file); j++)
		{
			fwrite(&fil[i*w2 + j].b, sizeof(char), 1, file);
			fwrite(&fil[i*w2 + j].g, sizeof(char), 1, file);
			fwrite(&fil[i*w2 + j].r, sizeof(char), 1, file);
		}
		for (int ii = 0; ii < trash; ii++)
			fwrite(&trashchar, sizeof(char), 1, file);
	}
}
__global__ void cudafilter(color * cudafil, color *cudapix, int group, int w1)
{
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned ar = 0, ag = 0, ab = 0;
	for(int i =0; i < group; i++)
		for(int j = 0; j < group; j++)
		{
			ar += cudapix[i + blockIdx.x*group*w1 + j + threadIdx.x*group].r;
			ag += cudapix[i + blockIdx.x*group*w1 + j + threadIdx.x*group].g;
			ab += cudapix[i + blockIdx.x*group*w1 + j +threadIdx.x*group].b;
		}
	cudafil[x].r = ar / (group*group);
	cudafil[x].g = ag / (group*group);
	cudafil[x].b = ab / (group*group);
}
void filter()
{
	w2 = w1 / group;
	h2 = h1 / group;
	fil = new color[w2*h2];
	
	color *cudafil, *cudapix;
	cudaMalloc((void**)&cudafil, w2 * h2 * sizeof(color));
	cudaMalloc((void**)&cudapix, w1 * h1 * sizeof(color));
	cudaMemcpy(cudapix, pix, w1 * h1 * sizeof(color), cudaMemcpyHostToDevice);
	cudafilter << < h2, w2 >> >(cudafil, cudapix, group, w1);
	cudaMemcpy(fil, cudafil, w2 * h2 * sizeof(color), cudaMemcpyDeviceToHost);
	cudaFree(cudafil);
	cudaFree(cudapix);
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	for (int i = 0; i < h1; i++)
		for (int j = 0; j < w1; j++)
		{
			glColor3ub(pix[i*w1 + j].r, pix[i*w1 + j].g, pix[i*w1 + j].b);
			glVertex2i(j, i);
		}
	glEnd();
	glFlush();
}
void display2()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	for (int i = 0; i < h2; i++)
		for (int j = 0; j < w2; j++)
		{
			glColor3ub(fil[i*w2 + j].r, fil[i*w2 + j].g, fil[i*w2 + j].b);
			glVertex2i(j, i);
		}
	glEnd();
	glFlush();
}
int main(int argc, char **argv)
{
	FILE *F;
	string name = "24bpp.bmp";
	cout << "Type the name:" << endl;
	cin >> name;
	fopen_s(&F, name.c_str(), "rb");
	int errorcode = readthefile(F);
	if (errorcode == 1)
	{
		fclose(F);
		return 0;
	}
	makepixmas(F);
	fclose(F);
	cout << "Type the group size:" << endl;
	cin >> group;
	filter();
	cout << "Type the name:" << endl;
	cin >> name;
	cudaDeviceReset();
	fopen_s(&F, name.c_str(), "wb");
	writethefile(F);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(bmpheader.width, bmpheader.height);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("kekes");
	glClearColor(0, 0, 0, 1);
	gluOrtho2D(0, bmpheader.width, 0, bmpheader.height);
	glutDisplayFunc(display);
	glutInitWindowSize(bmpheader.width / group, bmpheader.height / group);
	glutInitWindowPosition(bmpheader.width + 8, 0);
	glutCreateWindow("kekes2");
	glClearColor(0, 0, 0, 1);
	gluOrtho2D(0, bmpheader.width / group, 0, bmpheader.height / group);
	glutDisplayFunc(display2);
	glutMainLoop();
	if (pix != NULL)
		delete[]pix;
	if (fil != NULL) 
		delete[]fil;
    return 0;
}