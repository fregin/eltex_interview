#include <fstream>
#include <iostream>
#include <time.h>

using namespace std;

unsigned char signature[2];
unsigned int fileSize;
unsigned int reserved = 0;
unsigned int offset = 14 + 40;

unsigned int headerSize = 40;
unsigned int dimensions[2];
unsigned short colorPlanes = 1;
unsigned short bpp;
unsigned int compression = 0;
unsigned int imgSize;
unsigned int resolution[2];
unsigned int pltColors = 0;
unsigned int impColors = 0;
unsigned char **clrr, **clrg, **clrb, **clrx;
int padding, kek;
float sredntime;

void sredn(unsigned char **c, int i, int j, int n1, int n2)
{
	int cur = 0;
	int cnt = (n1 + 1)*(n2 + 1);

	for (int ii = i; ii <= i + n1; ii++)
		for (int jj = j; jj <= j + n2; jj++)
			cur += c[ii][jj];
	cur /= cnt;
	c[i][j] = cur;
}

void makesredn(int n)
{
	int n1, n2;

	bool idealx = false;
	bool idealy = false;

	if (dimensions[1] % n == 0)
		idealy = true;
	if (dimensions[0] % n == 0)
		idealx = true;

	for (int i = 0; i < dimensions[1]; i += n)
		for (int j = 0; j < dimensions[0]; j += n)
		{
			n1 = n2 = n - 1;

			if (!idealy && i + dimensions[1] % n == dimensions[1])
				n1 = dimensions[1] % n - 1;
			if (!idealx && j + dimensions[0] % n == dimensions[0])
				n2 = dimensions[0] % n - 1;

			sredn(clrr, i, j, n1, n2);
			sredn(clrg, i, j, n1, n2);
			sredn(clrb, i, j, n1, n2);
			if (bpp == 32)
				sredn(clrx, i, j, n1, n2);
		}
}

void createnew()
{
	char name[100];
	cout << "Name?" << endl;
	gets_s(name);
	ofstream os(name, ios::binary);
	float time0 = clock();

	dimensions[0] /= kek; dimensions[1] /= kek; fileSize = dimensions[0] * dimensions[1] * bpp / 8 + 54;
	imgSize = fileSize - 54;

	os.write(reinterpret_cast<char*>(signature), sizeof(signature));
	os.write(reinterpret_cast<char*>(&fileSize), sizeof(fileSize));
	os.write(reinterpret_cast<char*>(&reserved), sizeof(reserved));
	os.write(reinterpret_cast<char*>(&offset), sizeof(offset));
	os.write(reinterpret_cast<char*>(&headerSize), sizeof(headerSize));
	os.write(reinterpret_cast<char*>(dimensions), sizeof(dimensions));
	os.write(reinterpret_cast<char*>(&colorPlanes), sizeof(colorPlanes));
	os.write(reinterpret_cast<char*>(&bpp), sizeof(bpp));
	os.write(reinterpret_cast<char*>(&compression), sizeof(compression));
	os.write(reinterpret_cast<char*>(&imgSize), sizeof(imgSize));
	os.write(reinterpret_cast<char*>(resolution), sizeof(resolution));
	os.write(reinterpret_cast<char*>(&pltColors), sizeof(pltColors));
	os.write(reinterpret_cast<char*>(&impColors), sizeof(impColors));

	padding = dimensions[0] % 4;

	for (int i = 0; i / kek < dimensions[1]; i += kek)
	{
		for (int j = 0; j / kek < dimensions[0]; j += kek)
		{
			os.write(reinterpret_cast<char*>(&clrb[i][j]), sizeof(clrb[i][j]));
			os.write(reinterpret_cast<char*>(&clrg[i][j]), sizeof(clrg[i][j]));
			os.write(reinterpret_cast<char*>(&clrr[i][j]), sizeof(clrr[i][j]));
			if (bpp == 32)
				os.write(reinterpret_cast<char*>(&clrx[i][j]), sizeof(clrx[i][j]));
		}
		for (int k = 0; k < padding; k++)
		{
			char kk = '0';
			os.write(&kk, 1);
		}
	}

	os.close();
	float time1 = clock();
	cout << "Complete!" << endl << "It costs " << time1 - time0 + sredntime << "ms!" << endl;
}

void read()
{
	char name[100];
	cout << "Name?" << endl;
	gets_s(name);
	float time0 = clock();
	ifstream is(name, ios::binary);

	is.read(reinterpret_cast<char*>(signature), sizeof(signature));
	if (signature[0] != 'B' || signature[1] != 'M')
	{
		cout << "Invalid format!" << endl;
		return;
	}
	is.read(reinterpret_cast<char*>(&fileSize), sizeof(fileSize));
	is.read(reinterpret_cast<char*>(&reserved), sizeof(reserved));
	is.read(reinterpret_cast<char*>(&offset), sizeof(offset));

	is.read(reinterpret_cast<char*>(&headerSize), sizeof(headerSize));
	is.read(reinterpret_cast<char*>(dimensions), sizeof(dimensions));
	is.read(reinterpret_cast<char*>(&colorPlanes), sizeof(colorPlanes));
	is.read(reinterpret_cast<char*>(&bpp), sizeof(bpp));
	if (bpp != 24 && bpp != 32)
	{
		cout << "Wrong bpp!" << endl;
		return;
	}
	is.read(reinterpret_cast<char*>(&compression), sizeof(compression));
	is.read(reinterpret_cast<char*>(&imgSize), sizeof(imgSize));
	is.read(reinterpret_cast<char*>(resolution), sizeof(resolution));
	is.read(reinterpret_cast<char*>(&pltColors), sizeof(pltColors));
	is.read(reinterpret_cast<char*>(&impColors), sizeof(impColors));

	padding = dimensions[0] % 4;

	clrr = new unsigned char *[dimensions[1]];
	clrg = new unsigned char *[dimensions[1]];
	clrb = new unsigned char *[dimensions[1]];
	if (bpp == 32)
		clrx = new unsigned char *[dimensions[1]];
	clrr[0] = new unsigned char[dimensions[0]];
	clrg[0] = new unsigned char[dimensions[0]];
	clrb[0] = new unsigned char[dimensions[0]];
	if (bpp == 32)
		clrx[0] = new unsigned char[dimensions[0]];
	for (int i = 0; i < dimensions[1]; ++i)
	{
		for (int j = 0; j < dimensions[0]; ++j)
		{
			is.read(reinterpret_cast<char*>(&clrb[i][j]), sizeof(clrb[i][j]));
			is.read(reinterpret_cast<char*>(&clrg[i][j]), sizeof(clrg[i][j]));
			is.read(reinterpret_cast<char*>(&clrr[i][j]), sizeof(clrr[i][j]));
			if (bpp == 32)
				is.read(reinterpret_cast<char*>(&clrx[i][j]), sizeof(clrx[i][j]));
		}
		for (int k = 0; k < padding; k++)
		{
			char kk;
			is.read(&kk, sizeof(kk));
		}
		if (i != dimensions[1] - 1)
		{
			clrr[i + 1] = new unsigned char[dimensions[0]];
			clrg[i + 1] = new unsigned char[dimensions[0]];
			clrb[i + 1] = new unsigned char[dimensions[0]];
			if (bpp == 32)
				clrx[i + 1] = new unsigned char[dimensions[0]];
		}
	}

	cout << signature[0] << signature[1] << endl;
	cout << dimensions[0] << " " << dimensions[1] << endl;

	is.close();
	float time1 = clock();
	cout << "Complete!" << endl << "It costs " << time1 - time0 << "ms!" << endl;
}

void main()
{
	char type[100];
	cout << "read - to read the BITMAP" << endl << "create - to create new compacted BITMAP" << endl << "exit - to exit" << endl << "==========" << endl;
	while (1)
	{
		gets_s(type);
		if (!strcmp(type, "read"))
			read();
		else if (!strcmp(type, "create"))
		{
			if (signature[0] != 'B')
			{
				cout << "Haven't read any BITMAPs!" << endl;
				continue;
			}
			cout << "How pixels to compact?" << endl;
			cin >> kek;
			gets_s(type);
			float time00 = clock();
			makesredn(kek);
			float time11 = clock();
			sredntime = time11 - time00;
			createnew();
		}
		else if (!strcmp(type, "exit"))
		{
			for (int i = 0; i < dimensions[1]; ++i)
			{
				delete[] clrr[i];
				delete[] clrg[i];
				delete[] clrb[i];
				if (bpp == 32)
					delete[] clrx[i];
			}
			break;
		}
		else if (strcmp(type, ""))
			cout << "Unknown command!" << endl;
	}
}